const inquirer = require('inquirer');
var fs = require('fs');
var exec = require('child_process').exec, child;
const prompts = require('prompts');
const { execSync } = require('child_process');
const chalk = require('chalk');
var Spinner = require('cli-spinner').Spinner;
var spawn = require('child_process').spawn;

const functionsForReadingAndWritingToIniConfigFiles = require('./StartupScreen/functionsForReadingAndWritingToIniConfigFiles');

function limitStringLinesCount(string, numberOfLines){
    var stringToReturn = "";
    splittedString = string.split("\n")
    for(var i=0;i<numberOfLines;i++){
        stringToReturn = stringToReturn + splittedString[i] + "\n";
    }
    return stringToReturn;
}

function getSizeOfStringLines(string){
    var linesOfSring = string.split("\n");
    return linesOfSring.length;
}

var MakegitURLtoNormalURL = function (gitURl) 
{
    console.log("gitURl: " + gitURl);

    if (gitURl.includes("gitlab.com")) {
        var StringToReturn2 = gitURl.replace(/:/g, "/");
        return (StringToReturn2);
    }
    if (gitURl.includes("bitbucket.org")) {
        var pattern = /.*https:\/\/(.*)\@.*/gi;
        var StringToReturn1 = pattern.exec(gitURl);

        console.log("StringToReturn1: " + StringToReturn1);

        if (is_empty(StringToReturn1)){
            var pattern1 = /.*https:\/\/*/gi;
            var StringToReturn11 = pattern1.exec(gitURl);
            var StringToReturn21 = gitURl.replace(StringToReturn11[1], "").replace("@", "");
            return (StringToReturn21);
        }
        else{
            var StringToReturn2 = gitURl.replace(StringToReturn1[1], "").replace("@", "");
            return (StringToReturn2);
        }
    }
}

var MakegitURLtoNormalURL2 = function (gitURl) 
{
    console.log("gitURl: " + gitURl);

    if (gitURl.includes("gitlab.com")) {
        var StringToReturn2 = gitURl.replace('\n','') +".git";
        return (StringToReturn2);
    }
    if (gitURl.includes("bitbucket.org")) {
        var pattern = /.*https:\/\/(.*)\@.*/gi;
        var StringToReturn1 = pattern.exec(gitURl);

        console.log("StringToReturn1: " + StringToReturn1);

        if (is_empty(StringToReturn1)){
            var pattern1 = /.*https:\/\/*/gi;
            var StringToReturn11 = pattern1.exec(gitURl);
            var StringToReturn21 = gitURl.replace(StringToReturn11[1], "").replace("@", "");
            return (StringToReturn21);
        }
        else{
            var StringToReturn2 = gitURl.replace(StringToReturn1[1], "").replace("@", "");
            return (StringToReturn2);
        }
    }
}

function isNullOrEmpty(str){
    return !str||!str.trim();
}
var testEmpty = function (str) {
  if(isNullOrEmpty(str)){
    return 'empty-yes';
  } else {
    return 'empty-no';
  }
}

var is_empty = function (x) 
{
   return ( 
        (typeof x == 'undefined')
                    ||
        (x == null) 
                    ||
        (x == false)  //same as: !x
                    ||
        (x.length == 0)
                    ||
        (x == "")
                    ||
        (!/[^\s]/.test(x))
                    ||
        (/^\s*$/.test(x))
  );
}

function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}

var CleanString = function (StringValue) 
{
    var returnString = StringValue.toString()
        .replace(/\n|\0|\t|\r|\f|\v|\x0d|\x0a|\x0d\x0a|\↵/g, '')
        .replace("\\x00", null)
        .replace(/[^A-Za-z 0-9 \.,\?""!@#\$%\^&\*\(\)-_=\+;:<>\/\\\|\}\{\[\]`~]*/g, '')
        .replace(/(?![\x00-\x7F]|[\xC0-\xDF][\x80-\xBF]|[\xE0-\xEF][\x80-\xBF]{2}|[\xF0-\xF7][\x80-\xBF]{3})./g)
        .replace(/([\x7F-\x84]|[\x86-\x9F]|[\uFDD0-\uFDEF]|(?:\uD83F[\uDFFE\uDFFF])|(?:\uD87F[\uDFFE\uDFFF])|(?:\uD8BF[\uDFFE\uDFFF])|(?:\uD8FF[\uDFFE\uDFFF])|(?:\uD93F[\uDFFE\uDFFF])|(?:\uD97F[\uDFFE\uDFFF])|(?:\uD9BF[\uDFFE\uDFFF])|(?:\uD9FF[\uDFFE\uDFFF])|(?:\uDA3F[\uDFFE\uDFFF])|(?:\uDA7F[\uDFFE\uDFFF])|(?:\uDABF[\uDFFE\uDFFF])|(?:\uDAFF[\uDFFE\uDFFF])|(?:\uDB3F[\uDFFE\uDFFF])|(?:\uDB7F[\uDFFE\uDFFF])|(?:\uDBBF[\uDFFE\uDFFF])|(?:\uDBFF[\uDFFE\uDFFF])(?:[\0-\t\x0B\f\x0E-\u2027\u202A-\uD7FF\uE000-\uFFFF]|[\uD800-\uDBFF][\uDC00-\uDFFF]|[\uD800-\uDBFF](?![\uDC00-\uDFFF])|(?:[^\uD800-\uDBFF]|^)[\uDC00-\uDFFF]))/g, '');
    return returnString;
}

var uniq = function (a) 
{
    var prims = { "boolean": {}, "number": {}, "string": {} }, objs = [];

    return a.filter(function (item) {
        var type = typeof item;
        if (type in prims)
            return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true);
        else
            return objs.indexOf(item) >= 0 ? false : objs.push(item);
    });
}

var getUserHome = function () 
{
    const os = require('os');
    return os.homedir();
}

// var gitCloneFromUrlRetryOnFailure = function (nameOfRemoteRepo) 
// {
//     runCmdHandler(".", "start cmd /k git clone " + nameOfRemoteRepo + '.git');
// }

var spawnProcess = function (dir, cmd) 
{
    return (process.platform.toLowerCase().indexOf("win") >= 0) 
      ? spawnWindowsProcess(dir, cmd)
      : spawnLinuxProcess(dir, cmd);
}
    
var spawnWindowsProcess = function (dir, cmd) 
{
    return spawn("cmd.exe", ["/c", cmd], {cwd: dir});
}

var spawnLinuxProcess = function (dir, cmd) 
{
    var cmdParts = cmd.split(/\s+/);

    return spawn(cmdParts[0], cmdParts.slice(1), { cwd: dir});
}

var runCmdHandler = function (dir, cmd) 
{
    var process = null;
    try {
        process = spawnProcess(dir, cmd);
    } catch (e) {
        console.error("Error trying to execute command '" + cmd + "' in directory '" + dir + "'");
        console.error(e);
        console.log("error", e.message);
        console.log("finished");
        return;
    }
    process.stdout.on('data', function (data) {
        console.log("progress", data.toString('utf-8'));
    });
    process.stderr.on('data', function (data) {
        console.log("error", data.toString('utf-8'));
    });
    process.on('exit', function (code) {
        console.log("finished");
    });
}

var gitPush_ExternalCall = function () 
{
    // runCmdHandler(".", "start cmd /k git push");
    // try{
    child = exec("git push",
    { maxBuffer: 1024 * 1024 },
    function (error, stdout, stderr) {
        console.log(stdout);
        if (error == null ){
            console.log('ERROR: no operations have been made.');
            console.log('ERROR message:');
            console.log(error);
            console.log('');
        }
        if (error.toString().includes("fatal: TypeLoadException encountered.")) {
            runCmdHandler(".", "start cmd /k git push");
        }
        if (stderr !== null && !stderr.toString().includes("fatal: The current branch") && !stderr.toString().includes("fatal: TypeLoadException encountered.")) {
            console.log('stderr: ' + stderr);
        }
        if (error !== null && !error.toString().includes("fatal: The current branch") && !error.toString().includes("fatal: TypeLoadException encountered.")) {
            console.log('exec error: ' + error);
        }
        if (stderr.toString().includes("fatal: The current branch")) {
            // console.log('dddd');
            // git push --set-upstream origin TestingBranching2 

            child = exec("git rev-parse --abbrev-ref HEAD",
            { maxBuffer: 1024 * 1024 },
            function (error, stdout, stderr) {

                stdout = stdout.replace('\n','');

                console.log(' The current branch '+ stdout + ' has no upstream branch. Do you want to execute ' 
                + chalk.blue('git push --set-upstream origin ' + stdout ) + ' to create upstream branch? ');

                inquirer
                    .prompt([
                        {
                            type: 'list',
                            name: 'option',
                            pageSize: '40',
                            choices:  [
                                chalk.green('YES'),
                                chalk.red('NO')],
                        },
                    ])
                    .then(answers => {
                                    
                        if (answers.option == chalk.green('YES')) {
                            // console.log('YES');
                            // runCmdHandler(".", "start cmd /k git push --set-upstream origin " + stdout);
                            child = exec("git push --set-upstream origin " + stdout,
                                { maxBuffer: 1024 * 1024 },
                                function (error, stdout, stderr) {
                                    console.log(stdout);
                                    if (stderr !== null) {
                                        console.log('stderr: ' + stderr);
                                    }
                                    if (error !== null) {
                                        console.log('exec error: ' + error);
                                    }
                                    if (stderr === null && error === null) {

                                        console.log(' remote branch has been successfully created. Do you want to try to push now?');
                        
                                        inquirer
                                        .prompt([
                                            {
                                                type: 'list',
                                                name: 'option',
                                                pageSize: '40',
                                                choices:  [
                                                    chalk.green('YES'),
                                                    chalk.red('NO')],
                                            },
                                        ])
                                        .then(answers => {
                                                        
                                            if (answers.option == chalk.green('YES')) {
                                                // console.log('YES');
                                                runCmdHandler(".", "start cmd /k git push");
                                                
                                            }
                                            if (answers.option == chalk.red('NO')) {
                                                console.log(chalk.red('Ok'));
                                            }
                                        });

                                    }
                                });
                        }
                        if (answers.option == chalk.red('NO')) {
                            console.log(chalk.red('Ok, but without upstream branch, you cannot push to remote.'));
                        }
                    });

                });
        }
    });
    // }
    // catch(e){
    //     if (e.toString().includes("fatal: TypeLoadException encountered.")) {
    //         runCmdHandler(".", "start cmd /k git push");
    //     }
    // }

};

Array.prototype.clean = function (deleteValue) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] == deleteValue) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
};

var MakeTerminalCallFromMenuName = function (TerminalCall) 
{
    child = exec( 
           TerminalCall
        , { maxBuffer: 1024 * 1024 }
        , function (error, stdout, stderr) {
            console.log(stdout);
             if (testEmpty(stderr) !== 'empty-yes') {
                console.log('stderr: ' + stderr);
            }
            if (error !== null) {
                console.log('exec error: ' + error);
            }

            
            // const path=require('path');
            // let oneStepBack=path.join(__dirname,'../');
            // const main= require(oneStepBack+'easyGit.js');
            // console.log(oneStepBack+'easyGit.js');
            // main.main();

            // const main2= require('../easyGit.js');
            // main2.AppVersion();

            
            const main= require('../easyGit');

            main.main();

            // main2.main();
        }
        );
}

var OpenLocalRepo = function (answersOption)
{
    // if (answers.option == answersOption) {
        var opsys = process.platform;
        if (opsys == "darwin") {
            opsys = "MacOS";
        }
        else if (opsys == "win32" || opsys == "win64") {
            WinUserName = getUserHome();
            var dir = WinUserName + '/Documents/GitRepoList';

            var FileDir = dir + "/List.txt";
            if (fs.existsSync(FileDir)) {
                FileText = fs.readFileSync(dir + "/List.txt");
                var fileSet = uniq(FileText.toString().split(/\r?\n/).clean(''));
                // console.log(fileSet);

                inquirer
                    .prompt([
                        {
                            type: 'list',
                            name: 'option',
                            message: 'Choose an option:',
                            pageSize: '16',
                            choices: fileSet,
                        },
                    ])
                    .then(answers => {
                        var answersString = answers.option;
                        console.log(answersString);

                        dirString = answersString.split(" ");
                        console.log(dirString[0]);

                        require('child_process').exec('start "" ' + dirString[0]);

                    });

            }
        }
        else if (opsys == "linux") {
            LinuxUserName = getUserHome();
            var dir = LinuxUserName + '/GitRepoList';

            var FileDir = dir + "/List.txt";
            if (fs.existsSync(FileDir)) {
                FileText = fs.readFileSync(dir + "/List.txt");
                var fileSet = uniq(FileText.toString().split(/\r?\n/).clean(''));
                // console.log(fileSet);

                inquirer
                    .prompt([
                        {
                            type: 'list',
                            name: 'option',
                            message: 'Choose an option:',
                            pageSize: '16',
                            choices: fileSet,
                        },
                    ])
                    .then(answers => {
                        var answersString = answers.option;
                        console.log(answersString);

                        dirString = answersString.split(" ");
                        console.log(dirString[0]);
                        if (opsys == "darwin") {
                            opsys = "MacOS";
                        }
                        else if (opsys == "win32" || opsys == "win64") {
                            require('child_process').exec('start "" ' + dirString[0]);
                        }
                        else if (opsys == "linux") {
                            // This is made to work on Ubuntu/Mint
                            require('child_process').exec('nemo ' + dirString[0]);
                        }

                    });
            }
        }
    // }
}

var RegisterRepo = function (answersOption)
{
    // if (answers.option == answersOption) {

        var opsys = process.platform;
        if (opsys == "darwin") {
            opsys = "MacOS";
        } else if (opsys == "win32" || opsys == "win64") {
            WinUserName = getUserHome();
            console.log(WinUserName);
            opsys = "Windows";
            var dir = WinUserName + '/Documents/GitRepoList';

            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }

            var readableURL = "";
            child = exec('git config --get remote.origin.url',
                { maxBuffer: 1024 * 1024 },
                function (error, stdout, stderr) {
                    console.log(stdout);
                     if (testEmpty(stderr) !== 'empty-yes') {
                        console.log('stderr: ' + stderr);
                    }
                    readableURL = MakegitURLtoNormalURL(stdout);
                    var currentPath = __dirname;

                    var outputVar = currentPath + " " + readableURL;
                    console.log(outputVar);

                    try {
                        if (fs.existsSync(dir + "/List.txt")) {

                            FileText = fs.readFileSync(dir + "/List.txt");
                            var fileSet = uniq(FileText.toString().split(/\r?\n/).clean(''));

                            let uniqueArray = new Set();
                            fileSet.forEach(function (entry) {
                                uniqueArray.add(entry);
                            });

                            var stream = fs.createWriteStream(dir + "/List.txt");
                            stream.once('open', function (fd) {

                                stream.write(Array.from(uniqueArray).toString().replace("\r\n", "").replace(/,/g, "\r\n"));
                                stream.write("\r\n");
                                stream.write(outputVar);
                                stream.end();
                            });

                        }
                        else {
                            fs.writeFile(dir + "/List.txt", "", { flag: 'wx' }, function (err) {
                                console.log("File created");
                            });
                        }
                    }
                    catch (err) {

                    }
                });

        } else if (opsys == "linux") {
            LinuxUserName = getUserHome();
            console.log(LinuxUserName);

            var dir = LinuxUserName + '/GitRepoList';

            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }

            var readableURL = "";
            child = exec('git config --get remote.origin.url',
                { maxBuffer: 1024 * 1024 },
                function (error, stdout, stderr) {
                    console.log(stdout);
                     if (testEmpty(stderr) !== 'empty-yes') {
                        console.log('stderr: ' + stderr);
                    }
                    readableURL = MakegitURLtoNormalURL(stdout);
                    var currentPath = __dirname;

                    var outputVar = currentPath + " " + readableURL;
                    console.log(outputVar);

                    try {
                        if (fs.existsSync(dir + "/List.txt")) {

                            FileText = fs.readFileSync(dir + "/List.txt");
                            var fileSet = uniq(FileText.toString().split(/\r?\n/).clean(''));

                            let uniqueArray = new Set();
                            fileSet.forEach(function (entry) {
                                uniqueArray.add(entry);
                            });

                            var stream = fs.createWriteStream(dir + "/List.txt");
                            stream.once('open', function (fd) {


                                stream.write(Array.from(uniqueArray).toString().replace("\r\n", "").replace(/,/g, "\r\n"));
                                stream.write("\r\n");
                                stream.write(outputVar);
                                stream.end();
                            });

                        }
                        else {
                            fs.writeFile(dir + "/List.txt", "", { flag: 'wx' }, function (err) {
                                console.log("File created");
                            });
                        }
                    }
                    catch (err) {

                    }
                });
        }
    // }
}

var MakeNewBranches = function () 
{
    var arg1var;

    var questions1 = [{
        type: 'input',
        name: 'arg1',
        message: "Write the name of the new branch:",
    }]
    inquirer.prompt(questions1).then(answers => {
        arg1var = (`${answers['arg1']}`);
        
        var spinner = new Spinner('executing the git commands');
        spinner.setSpinnerString('|/-\\');
        spinner.start();

        child = exec('git checkout -b ' + arg1var,
        { maxBuffer: 1024 * 2048},
        function (error, stdout, stderr) {

            console.log('stdout: ' + stdout);
             if (testEmpty(stderr) !== 'empty-yes') {
                console.log('stderr: ' + stderr);
            }

            spinner.stop();
        });
    })
}

var SwitchBranches = function () 
{
    child = exec('git branch',
        { maxBuffer: 1024 * 1024 },
        function (error, stdout, stderr) {
            // console.log(stdout);
            if (testEmpty(stderr) !== 'empty-yes') {
                console.log('stderr: ' + stderr);
            }
            if (error !== null) {
                console.log('exec error: ' + error);
            }

            stdoutList = stdout.replace('*','').replace(' ','');
            stdoutList = stdoutList.replace(/[\x20]/g,"");
            stdoutList = stdoutList.split(/\r?\n/);
            stdoutList = stdoutList.filter(n => n)
            // console.log(stdoutList);

            inquirer
                .prompt([
                    {
                        type: 'list',
                        name: 'option',
                        message: 'Choose a branch:',
                        pageSize: '16',
                        choices: stdoutList,
                    },
                ])
                .then(answers => {
                    var answersString = answers.option;

                    child = exec('git checkout ' + answersString,
                        { maxBuffer: 1024 * 1024 },
                        function (error, stdout, stderr) {
                            console.log(stdout);
                             if (testEmpty(stderr) !== 'empty-yes') {
                                console.log('stderr: ' + stderr);
                            }
                            if (error !== null) {
                                console.log('exec error: ' + error);
                            }
                        });
                
                });
        });
}

var RemoveBranch = function () 
{
    child = exec('git branch',
        { maxBuffer: 1024 * 1024 },
        function (error, stdout, stderr) {
            // console.log(stdout);
            if (testEmpty(stderr) !== 'empty-yes') {
                console.log('stderr: ' + stderr);
            }
            if (error !== null) {
                console.log('exec error: ' + error);
            }

            stdoutList = stdout.replace('*','').replace(' ','');
            stdoutList = stdoutList.replace(/[\x20]/g,"");
            stdoutList = stdoutList.split(/\r?\n/);
            stdoutList = stdoutList.filter(n => n)
            // console.log(stdoutList);

            inquirer
                .prompt([
                    {
                        type: 'list',
                        name: 'option',
                        message: 'Choose a branch:',
                        pageSize: '16',
                        choices: stdoutList,
                    },
                ])
                .then(answers => {
                    var answersString = answers.option;

                    child = exec('git branch -D ' + answersString,
                        { maxBuffer: 1024 * 1024 },
                        function (error, stdout, stderr) {
                            console.log(stdout);
                             if (testEmpty(stderr) !== 'empty-yes') {
                                console.log('stderr: ' + stderr);
                            }
                            if (error !== null) {
                                console.log('exec error: ' + error);
                            }
                        });

                    // runCmdHandler(".", 'git branch \-\D ' + answersString);
                
                });
        });
}

var Removeremotebranch = function () 
{
    child = exec('git branch -a | grep remotes/*',
        { maxBuffer: 1024 * 1024 },
        function (error, stdout, stderr) {
            console.log(stdout);
            if (testEmpty(stderr) !== 'empty-yes') {
                console.log('stderr: ' + stderr);
            }
            if (error !== null) {
                console.log('exec error: ' + error);
            }

            stdoutList = stdout.replace('*','').replace(' ','');
            stdoutList = stdoutList.replace(/[\x20]/g,"");
            stdoutList = stdoutList.split(/\r?\n/);
            stdoutList = stdoutList.filter(n => n)

            var LinkedList = require('./linkedlist')

            var remoteNameForEntry = new LinkedList();
            var originNameForEntry = new LinkedList();
            var branchNameForEntry = new LinkedList();


            stdoutList.forEach(function(entry) {
                remoteNameForEntry.push(entry.split('/')[0]);
                originNameForEntry.push(entry.split('/')[1]);
                branchNameForEntry.push(entry.split('/')[2]);

            });

            var changeCheckRemoteNameForEntry = new LinkedList();
            while (remoteNameForEntry.next()) {
                if(remoteNameForEntry.current!=changeCheckRemoteNameForEntry.head){
                    changeCheckRemoteNameForEntry.push(remoteNameForEntry.current);
                }
            }

            var ArrayForchangeCheckRemoteNameForEntry = [];
            for (var i =0; i<changeCheckRemoteNameForEntry.length ;i++){
                changeCheckRemoteNameForEntry.next();
                ArrayForchangeCheckRemoteNameForEntry[i] = changeCheckRemoteNameForEntry.current;
                
            }


            var originNameForEntryForEntry = new LinkedList();
            while (originNameForEntry.next()) {
                if(originNameForEntry.current!=originNameForEntryForEntry.head){
                    originNameForEntryForEntry.push(originNameForEntry.current);
                }
            }

            var ArrayFororiginNameForEntryForEntry = [];
            for (var i =0; i<originNameForEntryForEntry.length ;i++){
                originNameForEntryForEntry.next();
                ArrayFororiginNameForEntryForEntry[i] = originNameForEntryForEntry.current;
                
            }

            var branchNameForEntryNameForEntry = new LinkedList();
            while (branchNameForEntry.next()) {
                if(branchNameForEntry.current!=branchNameForEntryNameForEntry.head){
                    branchNameForEntryNameForEntry.push(branchNameForEntry.current);
                }
            }

            var ArrayForbranchNameForEntryNameForEntry = [];
            for (var i =0; i<branchNameForEntryNameForEntry.length ;i++){
                branchNameForEntryNameForEntry.next();
                ArrayForbranchNameForEntryNameForEntry[i] = branchNameForEntryNameForEntry.current;
                
            }

            var chosenRemote = "";
            var chosenOrigin = "";
            var chosenBranch = "";

            inquirer
                .prompt([
                    {
                        type: 'list',
                        name: 'option',
                        message: 'Choose a remote:',
                        pageSize: '16',
                        choices: ArrayForchangeCheckRemoteNameForEntry,
                    },
                ])
                .then(answers => {
                    chosenRemote = answers.option;
                    inquirer
                        .prompt([
                            {
                                type: 'list',
                                name: 'option',
                                message: 'Choose an origin:',
                                pageSize: '16',
                                choices: ArrayFororiginNameForEntryForEntry,
                            },
                        ])
                        .then(answers => {
                            chosenOrigin = answers.option;
                            inquirer
                                .prompt([
                                    {
                                        type: 'list',
                                        name: 'option',
                                        message: 'Choose a branch:',
                                        pageSize: '16',
                                        choices: ArrayForbranchNameForEntryNameForEntry,
                                    },
                                ])
                                .then(answers => {
                                    chosenBranch = answers.option;

                                    console.log("chosenRemote: " + chosenRemote)
                                    console.log("chosenOrigin: " + chosenOrigin)
                                    console.log("chosenBranch: " + chosenBranch)

                                    // "git push " + chosenOrigin + " --delete " + chosenBranch
                                    child = exec("git push " + chosenOrigin + " --delete " + chosenBranch,
                                    { maxBuffer: 1024 * 1024 },
                                    function (error, stdout, stderr) {
                                        console.log(stdout);
                                            if (testEmpty(stderr) !== 'empty-yes') {
                                            console.log('stderr: ' + stderr);
                                        }
                                        if (error !== null) {
                                            console.log('exec error: ' + error);
                                        }

                                        if(stderr.includes("fatal: could not read Username for")){
                                            // console.log("Test");

                                                child = exec('start cmd /k git push ' + chosenOrigin + ' --delete ' + chosenBranch,
                                                { maxBuffer: 1024 * 1024 },
                                                function (error, stdout, stderr) {
                                                console.log(stdout);
                                                    if (testEmpty(stderr) !== 'empty-yes') {
                                                    console.log('stderr: ' + stderr);
                                                }
                                                if (error !== null) {
                                                    console.log('exec error: ' + error);
                                                }

                                                if(stderr.includes("fatal: could not read Username for")){
                                                    console.log("Test");

                                                }

                                            });
                                        }
                                    });
                            });
                    });
            });
        });
}

var MergeBranches = function () 
{
    // console.log('execggggg');
    child = exec('git branch',
        { maxBuffer: 1024 * 1024 },
        function (error, stdout, stderr) {
            // console.log(stdout);
            if (testEmpty(stderr) !== 'empty-yes') {
                console.log('stderr: ' + stderr);
            }
            if (error !== null) {
                console.log('exec error: ' + error);
            }

            stdoutList = stdout.replace('*','').replace(' ','');
            stdoutList = stdoutList.replace(/[\x20]/g,"");
            stdoutList = stdoutList.split(/\r?\n/);
            stdoutList = stdoutList.filter(n => n)
            // console.log(stdoutList);

            inquirer
                .prompt([
                    {
                        type: 'list',
                        name: 'option',
                        message: 'Choose a branch:',
                        pageSize: '16',
                        choices: stdoutList,
                    },
                ])
                .then(answers => {
                    var answersString = answers.option;

                    child = exec('git merge ' + answersString,
                        { maxBuffer: 1024 * 1024 },
                        function (error, stdout, stderr) {
                            console.log(stdout);
                             if (testEmpty(stderr) !== 'empty-yes') {
                                console.log('stderr: ' + stderr);
                            }
                            if (error !== null) {
                                console.log('exec error: ' + error);
                            }
                        });

                    // runCmdHandler(".", 'git branch \-\D ' + answersString);
                
                });
        });
}

var REVERTHeadAndLastCommit = function () 
{

    MakeTerminalCallFromMenuName('git reset HEAD~1');

}

var REVERTHeadAndLastCommitSoft = function () 
{

    // console.log('execggggg1');
    MakeTerminalCallFromMenuName('git reset --soft HEAD~1');

}

var REVERTHeadAndLastCommitHard = function () 
{

    // console.log('execggggg2');
    MakeTerminalCallFromMenuName('git reset --hard HEAD~1');

}


var TaggingShow = function () 
{
    // console.log('TaggingShow dddddddddddd');
    // console.log('execggggg');
    child = exec('git tag',
        { maxBuffer: 1024 * 1024 },
        function (error, stdout, stderr) {

            if (testEmpty(stderr) !== 'empty-yes') {
                console.log('stderr: ' + stderr);
            }
            if (error !== null) {
                console.log('exec error: ' + error);
            }

            stdoutList = stdout.replace('*','').replace(' ','');
            stdoutList = stdoutList.replace(/[\x20]/g,"");
            stdoutList = stdoutList.split(/\r?\n/);
            stdoutList = stdoutList.filter(n => n)

            inquirer
                .prompt([
                    {
                        type: 'list',
                        name: 'option',
                        message: 'Choose a branch:',
                        pageSize: '16',
                        choices: stdoutList,
                    },
                ])
                .then(answers => {
                    var answersString = answers.option;

                    child = exec('git show ' + answersString,
                        { maxBuffer: 1024 * 1024 },
                        function (error, stdout, stderr) {
                            console.log(stdout);
                             if (testEmpty(stderr) !== 'empty-yes') {
                                console.log('stderr: ' + stderr);
                            }
                            if (error !== null) {
                                console.log('exec error: ' + error);
                            }
                        });

                });
        });
}

var TaggingDeleteTags = function () 
{
    // console.log('TaggingShow dddddddddddd');
    // console.log('execggggg');
    child = exec('git tag',
        { maxBuffer: 1024 * 1024 },
        function (error, stdout, stderr) {

            if (testEmpty(stderr) !== 'empty-yes') {
                console.log('stderr: ' + stderr);
            }
            if (error !== null) {
                console.log('exec error: ' + error);
            }

            stdoutList = stdout.replace('*','').replace(' ','');
            stdoutList = stdoutList.replace(/[\x20]/g,"");
            stdoutList = stdoutList.split(/\r?\n/);
            stdoutList = stdoutList.filter(n => n)

            inquirer
                .prompt([
                    {
                        type: 'list',
                        name: 'option',
                        message: 'Choose a branch:',
                        pageSize: '16',
                        choices: stdoutList,
                    },
                ])
                .then(answers => {
                    var answersString = answers.option;

                    child = exec('git tag -d ' + answersString,
                        { maxBuffer: 1024 * 1024 },
                        function (error, stdout, stderr) {
                            console.log(stdout);
                             if (testEmpty(stderr) !== 'empty-yes') {
                                console.log('stderr: ' + stderr);
                            }
                            if (error !== null) {
                                console.log('exec error: ' + error);
                            }
                        });

                });
        });
}

var TaggingCreateAnnotatedTags = function () 
{
    var arg1var;
    var arg2var;

    var questions1 = [{
        type: 'input',
        name: 'arg1',
        message: "Input tag name (-a):",
        
    },{
        type: 'input',
        name: 'arg2',
        message: "Input tag message (-m):",
    }]

    // var questions2 = [{
    //     type: 'input',
    //     name: 'arg2',
    //     message: "Input tag message (-m):",
    // }]

    inquirer.prompt(questions1).then(answers => {
        arg1var = (`${answers['arg1']}`);
        arg2var = (`${answers['arg2']}`);
        
        var spinner = new Spinner('executing the git commands');
        spinner.setSpinnerString('|/-\\');
        spinner.start();

        child = exec('git tag -a ' +  arg1var + ' -m "' + arg2var +'"',
        { maxBuffer: 1024 * 2048},
        function (error, stdout, stderr) {
            console.log(stdout);
            if (testEmpty(stderr) !== 'empty-yes') {
               console.log('stderr: ' + stderr);
            }
            if (error !== null) {
               console.log('exec error: ' + error);
            }
            spinner.stop();
        });
    })
}

var TaggingCreateLightweightTags = function () 
{
    var arg1var;

    var questions1 = [{
        type: 'input',
        name: 'arg1',
        message: "Input argument 1:",
    }]
    inquirer.prompt(questions1).then(answers => {
        arg1var = (`${answers['arg1']}`);
        
        var spinner = new Spinner('executing the git commands');
        spinner.setSpinnerString('|/-\\');
        spinner.start();

        child = exec('git tag ' + arg1var,
        { maxBuffer: 1024 * 2048},
        function (error, stdout, stderr) {
            console.log(stdout);
            if (testEmpty(stderr) !== 'empty-yes') {
               console.log('stderr: ' + stderr);
            }
            if (error !== null) {
               console.log('exec error: ' + error);
            }
            spinner.stop();
        });
    })
}

var SETTINGSEnableordisablestartupdatafields = function () 
{
    const settingsStartupDataFieldsArray = ['AppVersion', 'gitBranches', 'gitStatus', 'localRepos'];

    var AppVersionVariable = functionsForReadingAndWritingToIniConfigFiles.
        ReadFromFile(settingsStartupDataFieldsArray[0],'./StartupScreen/easyGitConfig.ini');
    var gitBranchesVariable = functionsForReadingAndWritingToIniConfigFiles.
        ReadFromFile(settingsStartupDataFieldsArray[1],'./StartupScreen/easyGitConfig.ini');
    var gitStatusVariable = functionsForReadingAndWritingToIniConfigFiles.
        ReadFromFile(settingsStartupDataFieldsArray[2],'./StartupScreen/easyGitConfig.ini');
    var localReposVariable = functionsForReadingAndWritingToIniConfigFiles.
        ReadFromFile(settingsStartupDataFieldsArray[3],'./StartupScreen/easyGitConfig.ini');

    if(AppVersionVariable.localeCompare('Yes') === 0){
        AppVersionVariable = true;
    } else{
        AppVersionVariable = false;
    }

    if(gitBranchesVariable.localeCompare('Yes') === 0){
        gitBranchesVariable = true;
    } else{
        gitBranchesVariable = false;
    }

    if(gitStatusVariable.localeCompare('Yes') === 0){
        gitStatusVariable = true;
    } else{
        gitStatusVariable = false;
    }

    if(localReposVariable.localeCompare('Yes') === 0){
        localReposVariable = true;
    } else{
        localReposVariable = false;
    }

    inquirer.prompt([{
        type: 'checkbox',
        message: 'Select command line options',
        name: 'options',
        pageSize: '100',
        choices: [
        new inquirer.Separator('Choose which parts of the startup screen to show (use SPACE to check and unckeck): '),
            {
                name:settingsStartupDataFieldsArray[0],
                checked: AppVersionVariable
            },
            {
                name:settingsStartupDataFieldsArray[1],
                checked: gitBranchesVariable
            },
            {
                name:settingsStartupDataFieldsArray[2],
                checked: gitStatusVariable
            },        
            {
                name:settingsStartupDataFieldsArray[3],
                checked: localReposVariable
            },   
        ],
      }
    ])
    .then(answers => {
        console.log(answers.options);

        settingsStartupDataFieldsArray.forEach((element) => 
            functionsForReadingAndWritingToIniConfigFiles.WriteToFile('No',element,'./StartupScreen/easyGitConfig.ini'));

        answers.options.forEach(function(entry) {
            functionsForReadingAndWritingToIniConfigFiles.WriteToFile('Yes',entry,'./StartupScreen/easyGitConfig.ini')
        });
      
    });

}

var SETTINGSEnableordisableStatusVisualisationTypes = function () 
{
    const settingsArrayStatusVisualisationTypes = ['FileTracking', 'GitGraph', 'AbbreviatedCommits', 'CommitsPerDay'];

    var gitStatusTypeSimpleFilesTrackingVariable = functionsForReadingAndWritingToIniConfigFiles.
        ReadFromFile(settingsArrayStatusVisualisationTypes[0],'./StartupScreen/easyGitConfig.ini');
    var gitStatusTypeOnelineGraphDecorateColorShortstatVariable = functionsForReadingAndWritingToIniConfigFiles.
        ReadFromFile(settingsArrayStatusVisualisationTypes[1],'./StartupScreen/easyGitConfig.ini');
    var gitLogAbbreviatedCommitsInAshortstatOneLineGraphVariable = functionsForReadingAndWritingToIniConfigFiles.
        ReadFromFile(settingsArrayStatusVisualisationTypes[2],'./StartupScreen/easyGitConfig.ini');
    var ShowCommitsPerDayInANiceFormattedWayVariable = functionsForReadingAndWritingToIniConfigFiles.
        ReadFromFile(settingsArrayStatusVisualisationTypes[4],'./StartupScreen/easyGitConfig.ini');
    
    if(gitStatusTypeSimpleFilesTrackingVariable.localeCompare('Yes') === 0){
        gitStatusTypeSimpleFilesTrackingVariable = 'true';
    } else{
        gitStatusTypeSimpleFilesTrackingVariable = false;
    }

    if(gitStatusTypeOnelineGraphDecorateColorShortstatVariable.localeCompare('Yes') === 0){
        gitStatusTypeOnelineGraphDecorateColorShortstatVariable = 'true';
    } else{
        gitStatusTypeOnelineGraphDecorateColorShortstatVariable = false;
    }

    if(gitLogAbbreviatedCommitsInAshortstatOneLineGraphVariable.localeCompare('Yes') === 0){
        gitLogAbbreviatedCommitsInAshortstatOneLineGraphVariable = 'true';
    } else{
        gitLogAbbreviatedCommitsInAshortstatOneLineGraphVariable = false;
    }

    if(ShowCommitsPerDayInANiceFormattedWayVariable.localeCompare('Yes') === 0){
        ShowCommitsPerDayInANiceFormattedWayVariable = 'true';
    } else{
        ShowCommitsPerDayInANiceFormattedWayVariable = false;
    }

    inquirer.prompt([{
        type: 'checkbox',
        message: 'Select command line options',
        name: 'options',
        pageSize: '100',
        choices: [
        new inquirer.Separator('Choose which status types to be shown: '),
            {
                name:settingsArrayStatusVisualisationTypes[0],
                checked: eval(gitStatusTypeSimpleFilesTrackingVariable)
            },
            {
                name:settingsArrayStatusVisualisationTypes[1],
                checked: eval(gitStatusTypeOnelineGraphDecorateColorShortstatVariable)
            },
            {
                name:settingsArrayStatusVisualisationTypes[2],
                checked: eval(gitLogAbbreviatedCommitsInAshortstatOneLineGraphVariable)
            },
            {
                name:settingsArrayStatusVisualisationTypes[3],
                checked: eval(ShowCommitsPerDayInANiceFormattedWayVariable)
            },
        ],
      }
    ])
    .then(answers => {
        console.log(answers.options);

        settingsArrayStatusVisualisationTypes.forEach((element) => 
            functionsForReadingAndWritingToIniConfigFiles.WriteToFile('No',element,'./StartupScreen/easyGitConfig.ini'));
        
        answers.options.forEach(function(entry) {
            functionsForReadingAndWritingToIniConfigFiles.WriteToFile('Yes',entry,'./StartupScreen/easyGitConfig.ini')
        });

    });
}

var SETTINGSEnableordisableMiscellaneousSettings = function () 
{
    var topNumberOfLinesToDisplayVariable = functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('topNumberOfLinesToDisplay','./StartupScreen/easyGitConfig.ini');
    var truncationVariable = functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('truncation','./StartupScreen/easyGitConfig.ini');

    if (typeof truncationVariable !== 'undefined') {
        if(truncationVariable.localeCompare('Yes') === 0){
            truncationVariable = 'true';
        }
        if(truncationVariable.localeCompare('No') === 0){
            truncationVariable = 'false';
        }
    }
    else{
        truncationVariable = 'false';
    }

    inquirer.prompt([{
        type: 'checkbox',
        message: 'Select command line options',
        name: 'options',
pageSize: '100',
        choices: [
        new inquirer.Separator('Choose truncation level: '),
            {
                name: 'truncation',
                checked: eval(truncationVariable)
            },
        ],
      }
    ])
    .then(answers => {
        console.log(answers.options);

        functionsForReadingAndWritingToIniConfigFiles.WriteToFile('No','truncation','./StartupScreen/easyGitConfig.ini')

        answers.options.forEach(function(entry) {
            functionsForReadingAndWritingToIniConfigFiles.WriteToFile('Yes',entry,'./StartupScreen/easyGitConfig.ini')
        });

        inquirer.prompt([{
            type: 'number',
            message: 'Choose maximum number of lines displayed: ',
            name: 'maxNumberOfLines',
            pageSize: '100'
          }
        ])
        .then(answers => {
            var answersString2 = answers.maxNumberOfLines;
            console.log(answersString2);

            functionsForReadingAndWritingToIniConfigFiles.WriteToFile(
                answersString2,'topNumberOfLinesToDisplay','./StartupScreen/easyGitConfig.ini')
            }); 
        });
}

function splitString(str) {
    var middle = Math.ceil(str.length / 2);
    var s1 = str.slice(0, middle);
    return s1;
    };

// function validURL(str) {
//     var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
//         '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
//         '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
//         '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
//         '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
//         '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
//     return !!pattern.test(str);
//     }

function validURL(string) {
    var res = string.match(/(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g);
    return (res !== null)
}

module.exports = {
    limitStringLinesCount:limitStringLinesCount, 
    getSizeOfStringLines:getSizeOfStringLines,
    testEmpty: testEmpty,
    MakegitURLtoNormalURL: MakegitURLtoNormalURL,
    MakegitURLtoNormalURL2: MakegitURLtoNormalURL2,
    is_empty: is_empty,
    CleanString: CleanString,
    uniq: uniq,
    validURL:validURL,
    getUserHome: getUserHome,
    spawnProcess: spawnProcess,
    spawnWindowsProcess: spawnWindowsProcess,
    spawnLinuxProcess: spawnLinuxProcess,
    runCmdHandler: runCmdHandler,
    gitPush_ExternalCall: gitPush_ExternalCall,
    MakeTerminalCallFromMenuName: MakeTerminalCallFromMenuName,
    OpenLocalRepo: OpenLocalRepo,
    RegisterRepo: RegisterRepo,
    MakeNewBranches: MakeNewBranches,
    SwitchBranches: SwitchBranches,
    RemoveBranch: RemoveBranch,
    MergeBranches: MergeBranches,
    REVERTHeadAndLastCommit: REVERTHeadAndLastCommit,
    REVERTHeadAndLastCommitSoft: REVERTHeadAndLastCommitSoft,
    REVERTHeadAndLastCommitHard: REVERTHeadAndLastCommitHard,
    TaggingShow: TaggingShow,
    TaggingCreateAnnotatedTags: TaggingCreateAnnotatedTags,
    TaggingCreateLightweightTags: TaggingCreateLightweightTags,
    TaggingDeleteTags: TaggingDeleteTags,
    Removeremotebranch: Removeremotebranch,
    SETTINGSEnableordisablestartupdatafields: SETTINGSEnableordisablestartupdatafields,
    SETTINGSEnableordisableStatusVisualisationTypes: SETTINGSEnableordisableStatusVisualisationTypes,
    SETTINGSEnableordisableMiscellaneousSettings: SETTINGSEnableordisableMiscellaneousSettings,
    splitString:splitString
  }

//   gitCloneFromUrlRetryOnFailure: gitCloneFromUrlRetryOnFailure,
