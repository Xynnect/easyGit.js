const inquirer = require('inquirer');
var fs = require('fs');
var exec = require('child_process').exec, child;
const prompts = require('prompts');
const { execSync } = require('child_process');
const chalk = require('chalk');
var Spinner = require('cli-spinner').Spinner;
var spawn = require('child_process').spawn;
const puppeteer = require('puppeteer');

var opsys = process.platform;

const functions = require('./functions');

function removeDuplicates(arr) {
    return [...new Set(arr)];
}

function jsonConcat(o1, o2) {
    for (var key in o2) {
     o1[key] = o2[key];
    }
    return o1;
}

function isEmptyObject(value) {
    if (value == null) {
        // null or undefined
        return false;
    }

    if (typeof value !== 'object') {
        // boolean, number, string, function, etc.
        return false;
    }

    const proto = Object.getPrototypeOf(value);

    // consider `Object.create(null)`, commonly used as a safe map
    // before `Map` support, an empty object as well as `{}`
    if (proto !== null && proto !== Object.prototype) {
        return false;
    }

    return isEmpty(value);
}

async function scrapeData(pageIndex, url, email, password, username) {
    const browser = await puppeteer.launch({headless: "new"});
    let page = await browser.newPage();
    await page.setViewport({width: 1200, height: 720});
    await page.goto(url, { waitUntil: 'networkidle0' }); // wait until page load
    await page.type('#username', email);
    await page.click('#login-submit > span');

    await page
        .waitForSelector('#password', {visible: true})
        .then(() => page.type('#password', password));

    await page
        .waitForSelector('#login-submit > span', {visible: true})
        .then(() => page.click('#login-submit > span'));

    // await page.waitForNavigation({ waitUntil: 'networkidle2' });
    await page.waitForNavigation({ waitUntil: 'networkidle0' });
    await page.waitForNavigation({ waitUntil: 'domcontentloaded' });
    await page.goto("https://bitbucket.org/"+username+"/workspace/repositories/?page="+pageIndex);

    headings = await page
        .waitForSelector("td.css-1gtxc7f.edylmxf6 > div > div.css-wswknb.ebwdvol3 > a", {visible: true})
        .catch((e) => {
            console.error(); // "oh, no!"
          })
        .then(() => page.evaluate(() => {
            headings_elements = document.querySelectorAll("td.css-1gtxc7f.edylmxf6 > div > div.css-wswknb.ebwdvol3 > a")
            headings_array = Array.from(headings_elements);
            return headings_array.map(heading => heading.textContent);
            }));
    await browser.close();
    return headings;
}

function containsDuplicates(array) {
  var valuesSoFar = Object.create(null);
  for (var i = 0; i < array.length; ++i) {
      var value = array[i];
      if (value in valuesSoFar) {
          return true;
      }
      valuesSoFar[value] = true;
  }
  return false;
}

// Fetch data from multiple URLs concurrently and populate an array
async function populateArray(emailUsedForLogin, password, usernameOfDistinationDir) {
  // Create an array of URLs to scrape
  const urlsToScrape = [1,2,3,4,5];
  const dataArray = await Promise.all(urlsToScrape.map(async (pageIndex) => {
    const scrapedData = await scrapeData(pageIndex, 
                                        'https://id.atlassian.com/login?application=bitbucket&continue=https%3A%2F%2Fbitbucket.org%2Faccount%2Fsignin%2F%3Fnext%3D%252F%26redirectCount%3D1',
                                        emailUsedForLogin,
                                        password,
                                        usernameOfDistinationDir);
    return scrapedData;
  }));

  let array =[];
  dataArray.forEach((element) => {
    if (element.toString().localeCompare('') !== 0) {
      array.push(element);
    }
  });
  if(!containsDuplicates(array)){
    return array;
  }

  console.log("error");
  console.log("Trying again with same credentials");
  return populateArray(emailUsedForLogin, password, usernameOfDistinationDir);
}

module.exports = {
    ForcePush: (hashedDir) => {
        var dir="";
        if (hashedDir.localeCompare('') === 0) {
            dir = '';
        }
        else {
            dir = 'cd ' + hashedDir + ' && ';
        }
        child = exec(dir + "git push -f origin master",
        { maxBuffer: 1024 * 1024 },
        function (error, stdout, stderr) {
            console.log(stdout);
            if (error == null ){
                console.log('ERROR: nothing to push');
                console.log('ERROR: no operations have been made.');
                console.log('ERROR message:');
                console.log(error);
                console.log('');
            }
            else {
                if (error.toString().includes("fatal: TypeLoadException encountered.")) {
                    if(dir === ''){
                        dir = "."
                    }
                    runCmdHandler(dir, "start cmd /k git push -f origin master");
                    dir = '';
                }
                if (stderr !== null && !stderr.toString().includes("fatal: The current branch") && !stderr.toString().includes("fatal: TypeLoadException encountered.")) {
                    console.log('stderr: ' + stderr);
                }
                if (error !== null && !error.toString().includes("fatal: The current branch") && !error.toString().includes("fatal: TypeLoadException encountered.")) {
                    console.log('exec error: ' + error);
                }
                if (stderr.toString().includes("fatal: The current branch")) {
                    child = exec("git rev-parse --abbrev-ref HEAD",
                    { maxBuffer: 1024 * 1024 },
                    function (error, stdout, stderr) {

                        stdout = stdout.replace('\n','');

                        console.log(' The current branch '+ stdout + ' has no upstream branch. Do you want to execute ' 
                        + chalk.blue('git push -f origin master --set-upstream origin ' + stdout ) + ' to create upstream branch? ');

                        inquirer
                        .prompt([
                            {
                                type: 'list',
                                name: 'option',
                                pageSize: '40',
                                choices:  [
                                    chalk.green('YES'),
                                    chalk.red('NO')],
                            },
                        ])
                        .then(answers => {
                            if (answers.option == chalk.green('YES')) {
                                child = exec("git push -f origin master --set-upstream origin " + stdout,
                                { maxBuffer: 1024 * 1024 },
                                function (error, stdout, stderr) {
                                    console.log(stdout);
                                    if (stderr !== null) {
                                        console.log('stderr: ' + stderr);
                                    }
                                    if (error !== null) {
                                        console.log('exec error: ' + error);
                                    }
                                    if (stderr === null && error === null) {

                                        console.log(' remote branch has been successfully created. Do you want to try to push now?');
                        
                                        inquirer
                                        .prompt([
                                            {
                                                type: 'list',
                                                name: 'option',
                                                pageSize: '40',
                                                choices:  [
                                                    chalk.green('YES'),
                                                    chalk.red('NO')],
                                            },
                                        ])
                                        .then(answers => {
                                                        
                                            if (answers.option == chalk.green('YES')) {
                                                if(dir === ''){
                                                    dir = "."
                                                }
                                                runCmdHandler(dir, "start cmd /k git push -f origin master");
                                                dir = '';
                                            }
                                            if (answers.option == chalk.red('NO')) {
                                                console.log(chalk.red('Ok'));
                                            }
                                        });
                                    }
                                });
                            }
                            if (answers.option == chalk.red('NO')) {
                                console.log(chalk.red('Ok, but without upstream branch, you cannot push to remote.'));
                            }
                        });
                    });
                }
            }
        });
    },
    CommitAndPush: (hashedDir) => {
        const args = require('minimist')(process.argv.slice(2));
        var dir="";
        if (hashedDir.localeCompare('') === 0) {
            dir = '';
        }
        else {
            dir = 'cd ' + hashedDir + ' && ';
        }
        if (process.argv.length == 2) {
            var arg1var;
    
            var questions1 = [{
                type: 'input',
                name: 'arg1',
                message: "Input argument 1:",
            }]
            inquirer.prompt(questions1).then(answers => {
                arg1var = (`${answers['arg1']}`);
                
                var spinner = new Spinner('executing the git commands');
                spinner.setSpinnerString('|/-\\');
                spinner.start();
    
                child = exec(dir + 'git add . && git add -A  && git commit -m"' + arg1var + '" && start cmd /k git push',
                { maxBuffer: 1024 * 2048},
                function (error, stdout, stderr) {
                    console.log('stdout: ' + stdout);
                    if (functions.testEmpty(stderr) !== 'empty-yes') {
                        console.log('stderr: ' + stderr);
                    }
                    if (error !== null) {
                        console.log('exec error: ' + error);
    
                        if (String(error).includes("Error: Command failed:"))
                        {
                            functions.gitPush_ExternalCall()
                        }
                    }
                    if (String(stderr).includes("fatal: could not read Password for"))
                    {
                        functions.gitPush_ExternalCall()
                        
                    }
                    if (String(stderr).includes("Your branch is ahead of"))
                    {
                        functions.gitPush_ExternalCall()
                        
                    }
                    if (String(stderr).includes("failed to push some refs to"))
                    {
                        child = exec(dir + 'git add --ignore-errors . && git commit -m "initial commit" && git push -u origin master',
                        { maxBuffer: 1024 * 2048},
                        function (error, stdout, stderr) {
                            console.log('stdout: ' + stdout);
                            if (functions.testEmpty(stderr) !== 'empty-yes') {
                                console.log('stderr: ' + stderr);
                            }
                            
                        });
                    }
                    spinner.stop();
                });
            })
        }
        if (process.argv.length > 2) {
            var ArgumentStringTwo = JSON.stringify(args).slice(6, JSON.stringify(args).length - 2);
            var ArgumentStringThree = ArgumentStringTwo.split("\",\"").join(' ').slice(1, ArgumentStringTwo.split("\",\"").join(' ').length - 1);;
            console.log('ArgumentStringThree: ' + ArgumentStringThree);
            child = exec(dir + 'git add . && git add -A  && git commit -m"' + ArgumentStringThree + '" && start cmd /k git push',
                { maxBuffer: 1024 * 1024 },
                function (error, stdout, stderr) {
                    console.log('stdout: ' + stdout);
                    if (functions.testEmpty(stderr) !== 'empty-yes') {
                        console.log('stderr: ' + stderr);
                    }
                    if (error !== null) {
                        console.log('exec error: ' + error);
                    }
                });
        }
    },
    Sync_GitPush_ExternalCall: (hashedDir) => {
        functions.gitPush_ExternalCall(hashedDir);
    },
    Commit: (hashedDir) => {
        const args = require('minimist')(process.argv.slice(2));
        var dir="";
        if (hashedDir.localeCompare('') === 0) {
            dir = '';
        }
        else {
            dir = 'cd ' + hashedDir + ' && ';
        }
        if (process.argv.length == 2) {
            var arg1var;
    
            var questions1 = [{
                type: 'input',
                name: 'arg1',
                message: "Input argument 1:",
            }]
            inquirer.prompt(questions1).then(answers => {
                arg1var = (`${answers['arg1']}`);
                
                var spinner = new Spinner('executing the git commands');
                spinner.setSpinnerString('|/-\\');
                spinner.start();
    
                child = exec(dir + 'git add . && git add -A  && git commit -m"' + arg1var,
                { maxBuffer: 1024 * 2048},
                function (error, stdout, stderr) {
                    console.log('stdout: ' + stdout);
                    if (functions.testEmpty(stderr) !== 'empty-yes') {
                        console.log('stderr: ' + stderr);
                    }
                    if (error !== null) {
                        console.log('exec error: ' + error);
    
                        if (String(error).includes("Error: Command failed:"))
                        {
                            functions.gitPush_ExternalCall() 
                        }
                    }
                    if (String(stderr).includes("fatal: could not read Password for"))
                    {
                        functions.gitPush_ExternalCall()
                        
                    }
                    if (String(stderr).includes("Your branch is ahead of"))
                    {
                        functions.gitPush_ExternalCall()
                        
                    }
                    if (String(stderr).includes("failed to push some refs to"))
                    {
                        child = exec(dir + 'git add --ignore-errors . && git commit -m "initial commit"',
                        { maxBuffer: 1024 * 2048},
                        function (error, stdout, stderr) {
                            console.log('stdout: ' + stdout);
                            if (functions.testEmpty(stderr) !== 'empty-yes') {
                                console.log('stderr: ' + stderr);
                            }
                            
                        });
                    }
                    spinner.stop();
                });
            })
        }
        if (process.argv.length > 2) {
            var ArgumentStringTwo = JSON.stringify(args).slice(6, JSON.stringify(args).length - 2);
            var ArgumentStringThree = ArgumentStringTwo.split("\",\"").join(' ').slice(1, ArgumentStringTwo.split("\",\"").join(' ').length - 1);;
            console.log('ArgumentStringThree: ' + ArgumentStringThree);
            child = exec(dir + 'git add . && git add -A  && git commit -m"' + ArgumentStringThree,
            { maxBuffer: 1024 * 1024 },
            function (error, stdout, stderr) {
                console.log('stdout: ' + stdout);
                if (functions.testEmpty(stderr) !== 'empty-yes') {
                    console.log('stderr: ' + stderr);
                }
                if (error !== null) {
                    console.log('exec error: ' + error);
                }
            });
        }
    },
    GitPUllOriginMaster: (hashedDir) => {
        var dir="";
        if (hashedDir.localeCompare('') === 0) {
            dir = '';
        }
        else {
            dir = 'cd ' + hashedDir + ' && ';
        }
        child = exec(dir + 'git pull origin master',
        { maxBuffer: 1024 * 1024 },
        function (error, stdout, stderr) {
            console.log('stdout: ' + stdout);
            if (functions.testEmpty(stderr) !== 'empty-yes') {
                console.log('stderr: ' + stderr);
            }
            if (error !== null) {
                console.log('exec error: ' + error);
            }
            if (String(stderr).includes("fatal: could not read Password for"))
            {
                functions.gitPull_ExternalCall()
            }
            if (String(stderr).includes("Your branch is ahead of"))
            {
                functions.gitPull_ExternalCall()
            }
            if (String(stderr).includes('could not read Username'))
            {
                functions.gitPull_ExternalCall()
            }
        });
    },
    GitPUllRebase: (hashedDir) => {
        var dir="";
        if (hashedDir.localeCompare('') === 0) {
            dir = '';
        }
        else {
            dir = 'cd ' + hashedDir + ' && ';
        }
        child = exec(dir + 'git pull --rebase',
        { maxBuffer: 1024 * 1024 },
        function (error, stdout, stderr) {
            console.log('stdout: ' + stdout);
            if (functions.testEmpty(stderr) !== 'empty-yes') {
                console.log('stderr: ' + stderr);
            }
            if (error !== null) {
                console.log('exec error: ' + error);
            }
            if (String(stderr).includes("fatal: could not read Password for"))
            {
                functions.gitPull_ExternalCall()
            }
            if (String(stderr).includes("Your branch is ahead of"))
            {
                functions.gitPull_ExternalCall()
            }
            if (String(stderr).includes('could not read Username'))
            {
                functions.gitPull_ExternalCall()
            }
        });
    },
    GitPUll: (hashedDir) => {
        var dir="";
        if (hashedDir.localeCompare('') === 0) {
            dir = '';
        }
        else {
            dir = 'cd ' + hashedDir + ' && ';
        }
        child = exec(dir + 'git pull',
        { maxBuffer: 1024 * 1024 },
        function (error, stdout, stderr) {
            console.log('stdout: ' + stdout);
            if (functions.testEmpty(stderr) !== 'empty-yes') {
                console.log('stderr: ' + stderr);
            }
            if (error !== null) {
                console.log('exec error: ' + error);
            }
            if (String(stderr).includes("fatal: could not read Password for"))
            {
                if(dir === ''){
                    dir = "."
                }
                functions.runCmdHandler(dir, "start cmd /k git pull origin master");
                dir = '';
            }
            if (String(stderr).includes("Your branch is ahead of"))
            {
                if(dir === ''){
                    dir = "."
                }
                functions.runCmdHandler(dir, "start cmd /k git pull origin master");
                dir = '';
            }
            if (String(stderr).includes('could not read Username'))
            {
                if(dir === ''){
                    dir = "."
                }
                functions.runCmdHandler(dir, "start cmd /k git pull origin master");
                dir = '';
            } 
        });
    },
    GitCloneFromURl: (hashedDir) => {
        var dir="";
        if (hashedDir.localeCompare('') === 0) {
            dir = '';
        }
        else {
            dir = 'cd ' + hashedDir + ' && ';
        }
        const questions = [
            {
                type: 'text',
                name: 'url',
                pageSize: '50',
                message: 'What is the URL of the repository?'
            }
        ];
        (async () => {
            const response = await prompts(questions);
            child = exec(dir + 'git clone ' + response.url,
            { maxBuffer: 1024 * 1024 },
            function (error, stdout, stderr) {
                console.log('stdout: ' + stdout);
                if (functions.testEmpty(stderr) !== 'empty-yes') {
                    console.log('stderr: ' + stderr);
                }
                if (error !== null) {
                    console.log('exec error: ' + error);
                    console.log('response.url ' + response.url + '.git');
                    if(dir === ''){
                        dir = "."
                    }
                    functions.runCmdHandler(dir, "start cmd /k git clone " + response.url + '.git');
                    dir = '';
                }
            });
        })();
    },
    GetListOfRepos: (hashedDir, answers) => {
        PseudoBooleanOperationControl = "EndHerePseudo-BooleanNo";
        var dir="";
        if (hashedDir.localeCompare('') === 0) {
            dir = '';
        }
        else {
            dir = 'cd ' + hashedDir + ' && ';
        }
        inquirer
        .prompt([
            {
                type: 'list',
                name: 'option',
                message: 'Choose an option:',
                pageSize: '16',
                choices: ['Github' ,
                'Gitlab', 
                'Bitbucket', 
                'Gitea'],
            },
        ])
        .then(answers => {
            if (answers.option == "Github") {
                inquirer
                .prompt([
                    {
                        type: 'list',
                        name: 'option',
                        message: 'Choose an option:',
                        pageSize: '16',
                        choices: ['Github | old style - no token', 
                        'Github | new style - public repos - with token on timeout',
                        'Github | new style - public repos - with forced token',
                        'Github | new style - public and private repos - token needed'
                    ],
                    },
                ])
                .then(answers => {
                    if (answers.option == "Github | old style - no token") {
                        const questions = [
                            {
                                type: 'text',
                                name: 'username',
                                pageSize: '50',
                                message: 'What is your username?'
                            },
                            {
                                type: 'password',
                                name: 'password',
                                message: 'What is your password?'
                            }
                        ];
                        (async () => {
                            const response = await prompts(questions);

                            let output = "";
                            let emptyLines = 0;
                            for (var i = 0; i < 10; i++) {
                                var execSyncOutput = execSync(dir + 'curl -u' + '"' + response.username + ':' + response.password + '" '
                                    + '"https://api.github.com/user/repos?page="' + [i] + '"&per_page=100"');
                                console.log(dir + 'curl -u' + '"' + response.username + ':' + response.password + '" '
                                + '"https://api.github.com/user/repos?page="' + [i] + '"&per_page=100"');
                                if (execSyncOutput.toString().slice(0, -1).length < 5) {
                                    emptyLines += 1;
                                }
                                try {
                                    output = output.concat(execSyncOutput.toString().slice(0, -1));
                                }
                                catch (err) {
                                }
                            }
                            // console.log(emptyLines);
                            // console.log(emptyLines * 3 + 1);
                            var outputFromCurl = output.replace(/\]\[/g, ',').toString().slice(0, -(emptyLines * 3 + 1)) + "]";

                            var db = JSON.stringify(outputFromCurl);
                            var objstdout = JSON.parse(db);
                            // var objstdout = JSON.parse(outputFromCurl);

                            var gitURLList = [];
                            console.log(objstdout);

                            for (ii = 0; ii < objstdout.length; ii++) {
                                gitURLList.push(objstdout[ii].html_url);
                            }
                            for (var i = 0; i < gitURLList.length; i++) {
                                gitURLList[i] = i + ". " + gitURLList[i];
                            }
                            if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanYes") {
                                inquirer
                                .prompt([
                                    {
                                        type: 'list',
                                        name: 'option',
                                        message: 'Choose an option:',
                                        pageSize: '50',
                                        choices: gitURLList,
                                    },
                                ])
                                .then(answers => {
                                    var answersString = answers.option;
                                    console.log(answersString);

                                    dirString = answersString.split(" ");
                                    console.log(dirString[0]);
                                    console.log(dirString[1]);
                                    if (opsys == "darwin") {
                                        opsys = "MacOS";
                                    }
                                    else if (opsys == "win32" || opsys == "win64") {
                                        console.log(answers.option);

                                    }
                                    else if (opsys == "linux") {
                                        // This is made to work on Ubuntu/Mint
                                        console.log(answers.option);

                                    }
                                });
                            }
                            if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanNo") {
                                inquirer
                                    .prompt([
                                        {
                                            type: 'list',
                                            name: 'option',
                                            message: 'Choose an option:',
                                            pageSize: '50',
                                            choices: gitURLList,
                                        },
                                    ])
                                    .then(answers => {
                                        var answersString = answers.option;
                                        var opsys = process.platform;
                                        console.log(answersString);

                                        dirString = answersString.split(" ");
                                        console.log(dirString[0]);
                                        if (opsys == "darwin") {
                                            opsys = "MacOS";
                                        }
                                        else if (opsys == "win32" || opsys == "win64") {

                                            child = exec(dir + 'git clone ' + dirString[1],
                                                { maxBuffer: 1024 * 1024 },
                                                function (error, stdout, stderr) {
                                                    console.log(stdout);
                                                    if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                        console.log('stderr: ' + stderr);
                                                    }
                                                    if (error !== null) {
                                                        console.log('exec error: ' + error);

                                                        // If it fails
                                                        // functions.gitCloneFromUrlRetryOnFailure(dirString[1])
                                                        if(dir === ''){
                                                            dir = "."
                                                        }
                                                        functions.runCmdHandler(dir, "start cmd /k git clone " + dirString[1] + '.git');
                                                        dir = '';
                                                    }
                                                });
                                        }
                                        else if (opsys == "linux") {
                                            child = exec(dir + 'git clone ' + dirString[1],
                                                { maxBuffer: 1024 * 1024 },
                                                function (error, stdout, stderr) {
                                                    console.log(stdout);
                                                    if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                        console.log('stderr: ' + stderr);
                                                    }
                                                    if (error !== null) {
                                                        console.log('exec error: ' + error);
                                                    }
                                                });

                                        }
                                    });
                            }
                        })();
                    }
                    if (answers.option == "Github | new style - public repos - with token on timeout") {
                        const questions = [
                            {
                                type: 'text',
                                name: 'username',
                                pageSize: '50',
                                message: 'What is your repo account username?'
                            }
                        ];
                        (async () => {
                            const response = await prompts(questions);
                            // for (var i = 0; i < 10; i++) {
                            //     // curl https://api.github.com/users/Dian-Yordanov/repos
                            //     var execSyncOutput = execSync(dir + 'curl ' + 'https://api.github.com/users/' + response.username + '/repos');
                            // }

                            // objstdout = JSON.parse(execSyncOutput);

                            var output = new Array();
                            var execSyncOutput2 = '';
                            var jsonParsed = '';
                            for (var i = 0; i < 10; i++) {
                                execSyncOutput2 = execSync(dir + 'curl ' + '"https://api.github.com/users/' + response.username + '/repos?page='+ [i] + '&per_page=100"');
                                try { 
                                    jsonParsed = JSON.parse(execSyncOutput2);
                                    
                                    if(!functions.is_empty(jsonParsed)){
                                        output.push(jsonParsed);
                                    }
                                }
                                catch (err) {
                                }
                            }
                            var output2 = new Array();

                            for (iii = 0; iii < output.length; iii++) {
                                // console.log(output[iii]);
                                output2 = [].concat(output[iii]);
                            }

                            var gitURLList = [];

                            for (ii = 0; ii < output2.length; ii++) {
                                // console.log(output2[ii].html_url);
                                gitURLList.push(output2[ii].html_url);
                            }
                            for (var i = 0; i < gitURLList.length; i++) {
                                gitURLList[i] = i + ". " + gitURLList[i];
                            }

                            if(JSON.stringify(output).includes('API rate limit exceeded')){
                                console.log(chalk.redBright('You are being rate limited'));
                                console.log(chalk.redBright('The way to circumvent this would be to use a token from https://github.com/settings/tokens'));
                                console.log(chalk.redBright('The token is 40 characters long and only shown when created.'));
                                
                                const questions2 = [
                                    {
                                        type: 'text',
                                        name: 'token',
                                        pageSize: '50',
                                        message: 'What is your token?'
                                    }
                                ];
                                (async () => {
                                    const response2 = await prompts(questions2);

                                    var output = new Array();
                                    var execSyncOutput2 = '';
                                    for (var i = 0; i < 10; i++) {
                                        execSyncOutput2 = execSync(dir + 'curl -u ' + response2.token + ':x-oauth-basic "https://api.github.com/users/' + response.username + '/repos?page='+ [i] + '&per_page=100"');
                                        try { 
                                            var jsonParsed = JSON.parse(execSyncOutput2);
                                            
                                            if(!functions.is_empty(jsonParsed)){
                                                output.push(jsonParsed);
                                            }
                                        }
                                        catch (err) {
                                        }
                                    }
                                    var output2 = new Array();
                
                                    for (iii = 0; iii < output.length; iii++) {
                                        console.log(output[iii]);
                                        output2 = [].concat(output[iii]);
                                    }
                
                                    var gitURLList = [];
                
                                    for (ii = 0; ii < output2.length; ii++) {
                                        // console.log(output2[ii].html_url);
                                        gitURLList.push(output2[ii].html_url);
                                    }
                                    for (var i = 0; i < gitURLList.length; i++) {
                                        gitURLList[i] = i + ". " + gitURLList[i];
                                    }
                                    if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanYes") {
                                        inquirer
                                        .prompt([
                                            {
                                                type: 'list',
                                                name: 'option',
                                                message: 'Choose an option:',
                                                pageSize: '50',
                                                choices: gitURLList,
                                            },
                                        ])
                                        .then(answers => {
                                            var answersString = answers.option;
                                            console.log(answersString);
                
                                            dirString = answersString.split(" ");
                                            console.log(dirString[0]);
                                            console.log(dirString[1]);
                                            if (opsys == "darwin") {
                                                opsys = "MacOS";
                                            }
                                            else if (opsys == "win32" || opsys == "win64") {
                                                console.log(answers.option);
                
                                            }
                                            else if (opsys == "linux") {
                                                // This is made to work on Ubuntu/Mint
                                                console.log(answers.option);
                
                                            }
                                        });
                                    }
                                    if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanNo") {
                                        inquirer
                                            .prompt([
                                                {
                                                    type: 'list',
                                                    name: 'option',
                                                    message: 'Choose an option:',
                                                    pageSize: '50',
                                                    choices: gitURLList,
                                                },
                                            ])
                                            .then(answers => {
                                                var answersString = answers.option;
                                                var opsys = process.platform;
                                                console.log(answersString);
                
                                                dirString = answersString.split(" ");
                                                console.log(dirString[0]);
                                                if (opsys == "darwin") {
                                                    opsys = "MacOS";
                                                }
                                                else if (opsys == "win32" || opsys == "win64") {
                
                                                    child = exec(dir + 'git clone ' + dirString[1],
                                                        { maxBuffer: 1024 * 1024 },
                                                        function (error, stdout, stderr) {
                                                            console.log(stdout);
                                                            if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                                console.log('stderr: ' + stderr);
                                                            }
                                                            if (error !== null) {
                                                                console.log('exec error: ' + error);
                
                                                                // If it fails
                                                                // functions.gitCloneFromUrlRetryOnFailure(dirString[1])
                                                                if(dir === ''){
                                                                    dir = "."
                                                                }
                                                                functions.runCmdHandler(dir, "start cmd /k git clone " + dirString[1] + '.git');
                                                                dir = '';
                                                            }
                                                        });
                                                }
                                                else if (opsys == "linux") {
                                                    child = exec(dir + 'git clone ' + dirString[1],
                                                        { maxBuffer: 1024 * 1024 },
                                                        function (error, stdout, stderr) {
                                                            console.log(stdout);
                                                            if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                                console.log('stderr: ' + stderr);
                                                            }
                                                            if (error !== null) {
                                                                console.log('exec error: ' + error);
                                                            }
                                                        });
                
                                                }
                                            });
                                    }     
                                })();

                            }
                            else{
                                console.log(chalk.greenBright('Success!'));
                                // console.log(output2);

                                // var gitURLList = [];

                                // for (ii = 0; ii < output2.length; ii++) {
                                //     console.log(output2[ii].html_url);
                                //     gitURLList.push(output2[ii].html_url);
                                // }
                                // for (var i = 0; i < gitURLList.length; i++) {
                                //     gitURLList[i] = i + ". " + gitURLList[i];
                                // }
                                if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanYes") {
                                    inquirer
                                    .prompt([
                                        {
                                            type: 'list',
                                            name: 'option',
                                            message: 'Choose an option:',
                                            pageSize: '50',
                                            choices: gitURLList,
                                        },
                                    ])
                                    .then(answers => {
                                        var answersString = answers.option;
                                        console.log(answersString);
            
                                        dirString = answersString.split(" ");
                                        console.log(dirString[0]);
                                        console.log(dirString[1]);
                                        if (opsys == "darwin") {
                                            opsys = "MacOS";
                                        }
                                        else if (opsys == "win32" || opsys == "win64") {
                                            console.log(answers.option);
            
                                        }
                                        else if (opsys == "linux") {
                                            // This is made to work on Ubuntu/Mint
                                            console.log(answers.option);
            
                                        }
                                    });
                                }
                                if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanNo") {
                                    inquirer
                                        .prompt([
                                            {
                                                type: 'list',
                                                name: 'option',
                                                message: 'Choose an option:',
                                                pageSize: '50',
                                                choices: gitURLList,
                                            },
                                        ])
                                        .then(answers => {
                                            var answersString = answers.option;
                                            var opsys = process.platform;
                                            console.log(answersString);
            
                                            dirString = answersString.split(" ");
                                            console.log(dirString[0]);
                                            if (opsys == "darwin") {
                                                opsys = "MacOS";
                                            }
                                            else if (opsys == "win32" || opsys == "win64") {
            
                                                child = exec(dir + 'git clone ' + dirString[1],
                                                    { maxBuffer: 1024 * 1024 },
                                                    function (error, stdout, stderr) {
                                                        console.log(stdout);
                                                        if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                            console.log('stderr: ' + stderr);
                                                        }
                                                        if (error !== null) {
                                                            console.log('exec error: ' + error);
            
                                                            // If it fails
                                                            // functions.gitCloneFromUrlRetryOnFailure(dirString[1])
                                                            if(dir === ''){
                                                                dir = "."
                                                            }
                                                            functions.runCmdHandler(dir, "start cmd /k git clone " + dirString[1] + '.git');
                                                            dir = '';
                                                        }
                                                    });
                                            }
                                            else if (opsys == "linux") {
                                                child = exec(dir + 'git clone ' + dirString[1],
                                                    { maxBuffer: 1024 * 1024 },
                                                    function (error, stdout, stderr) {
                                                        console.log(stdout);
                                                        if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                            console.log('stderr: ' + stderr);
                                                        }
                                                        if (error !== null) {
                                                            console.log('exec error: ' + error);
                                                        }
                                                    });
            
                                            }
                                        });
                                }
                            }


                        })();
                    }
                    if (answers.option == "Github | new style - public repos - with forced token") {
                        console.log(chalk.magentaBright('You can get a token from https://github.com/settings/tokens'));
                        console.log(chalk.magentaBright('The token is 40 characters long and only shown when created.'));
                        const questions = [
                            {
                                type: 'text',
                                name: 'username',
                                pageSize: '50',
                                message: 'What is your repo account username?'
                            },
                            {
                                type: 'text',
                                name: 'token',
                                message: 'What is your token?'
                            }
                        ];
                        (async () => {
                            const response = await prompts(questions);

                            var output = new Array();
                            var execSyncOutput2 = '';
                            for (var i = 0; i < 10; i++) {
                                execSyncOutput2 = execSync(dir + 'curl -u ' + response.token + ':x-oauth-basic "https://api.github.com/users/' + response.username + '/repos?page='+ [i] + '&per_page=100"');
                                try { 
                                    var jsonParsed = JSON.parse(execSyncOutput2);
                                    
                                    if(!functions.is_empty(jsonParsed)){
                                        output.push(jsonParsed);
                                    }
                                }
                                catch (err) {
                                }
                            }
                            var output2 = new Array();

                            for (iii = 0; iii < output.length; iii++) {
                                // console.log(output[iii]);
                                output2 = [].concat(output[iii]);
                            }

                            var gitURLList = [];

                            for (ii = 0; ii < output2.length; ii++) {
                                // console.log(output2[ii].html_url);
                                gitURLList.push(output2[ii].html_url);
                            }
                            for (var i = 0; i < gitURLList.length; i++) {
                                gitURLList[i] = i + ". " + gitURLList[i];
                            }
                            if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanYes") {
                                inquirer
                                .prompt([
                                    {
                                        type: 'list',
                                        name: 'option',
                                        message: 'Choose an option:',
                                        pageSize: '50',
                                        choices: gitURLList,
                                    },
                                ])
                                .then(answers => {
                                    var answersString = answers.option;
                                    console.log(answersString);

                                    dirString = answersString.split(" ");
                                    console.log(dirString[0]);
                                    console.log(dirString[1]);
                                    if (opsys == "darwin") {
                                        opsys = "MacOS";
                                    }
                                    else if (opsys == "win32" || opsys == "win64") {
                                        console.log(answers.option);

                                    }
                                    else if (opsys == "linux") {
                                        // This is made to work on Ubuntu/Mint
                                        console.log(answers.option);

                                    }
                                });
                            }
                            if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanNo") {
                                inquirer
                                    .prompt([
                                        {
                                            type: 'list',
                                            name: 'option',
                                            message: 'Choose an option:',
                                            pageSize: '50',
                                            choices: gitURLList,
                                        },
                                    ])
                                    .then(answers => {
                                        var answersString = answers.option;
                                        var opsys = process.platform;
                                        console.log(answersString);

                                        dirString = answersString.split(" ");
                                        console.log(dirString[0]);
                                        if (opsys == "darwin") {
                                            opsys = "MacOS";
                                        }
                                        else if (opsys == "win32" || opsys == "win64") {

                                            child = exec(dir + 'git clone ' + dirString[1],
                                                { maxBuffer: 1024 * 1024 },
                                                function (error, stdout, stderr) {
                                                    console.log(stdout);
                                                    if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                        console.log('stderr: ' + stderr);
                                                    }
                                                    if (error !== null) {
                                                        console.log('exec error: ' + error);

                                                        // If it fails
                                                        // functions.gitCloneFromUrlRetryOnFailure(dirString[1])
                                                        if(dir === ''){
                                                            dir = "."
                                                        }
                                                        functions.runCmdHandler(dir, "start cmd /k git clone " + dirString[1] + '.git');
                                                        dir = '';
                                                    }
                                                });
                                        }
                                        else if (opsys == "linux") {
                                            child = exec(dir + 'git clone ' + dirString[1],
                                                { maxBuffer: 1024 * 1024 },
                                                function (error, stdout, stderr) {
                                                    console.log(stdout);
                                                    if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                        console.log('stderr: ' + stderr);
                                                    }
                                                    if (error !== null) {
                                                        console.log('exec error: ' + error);
                                                    }
                                                });

                                        }
                                    });
                            }     
                        })();
                    }
                    if (answers.option == "Github | new style - public and private repos - token needed") {
                        console.log(chalk.magentaBright('You can get a token from https://github.com/settings/tokens'));
                        console.log(chalk.magentaBright('The token is 40 characters long and only shown when created.'));
                        const questions = [
                            {
                                type: 'text',
                                name: 'username',
                                pageSize: '50',
                                message: 'What is your repo account username?'
                            },
                            {
                                type: 'text',
                                name: 'token',
                                message: 'What is your token?'
                            }
                        ];
                        (async () => {
                            const response = await prompts(questions);

                            var output = new Array();
                            var execSyncOutput2 = '';

                            let getReposCoountSync = execSync(dir + 'curl -u ' + response.token + ':x-oauth-basic "https://api.github.com/search/repositories?q=user:' + response.username + '"');
                            let totalRepoCount = JSON.parse(getReposCoountSync).total_count;
                            let cycleRuns = Math.ceil(totalRepoCount / 100);

                            for (var i = 0; i < cycleRuns; i++) {
                                execSyncOutput2 = execSync(dir + 'curl -u ' + response.token + ':x-oauth-basic "https://api.github.com/search/repositories?q=user:' + response.username + '&per_page=100&page='+ [i] + '"');
                                try { 
                                    var jsonParsed = JSON.parse(execSyncOutput2);
                                    
                                    if(!functions.is_empty(jsonParsed)){
                                        output.push(jsonParsed);
                                    }
                                }
                                catch (err) {
                                }
                            }
                            var output2 = new Array();
                            
                            for (iii = 0; iii < output.length; iii++) {
                                for (iiii = 0; iiii < output[iii].items.length; iiii++) {
                                    output2 = output2.concat(output[iii].items[iiii]);
                                }
                            }

                            var gitURLList = [];

                            for (ii = 0; ii < output2.length; ii++) {
                                gitURLList.push(output2[ii].visibility + " - " + output2[ii].html_url);
                            }

                            gitURLList = removeDuplicates(gitURLList);

                            for (var i = 0; i < gitURLList.length; i++) {

                                gitURLList[i] = i + ". " + gitURLList[i];
                            }

                            inquirer
                            .prompt([
                                {
                                    type: 'list',
                                    name: 'option',
                                    message: 'Choose an option:',
                                    pageSize: '50',
                                    choices: gitURLList,
                                },
                            ])
                            .then(answers => {
                                var answersString = answers.option;
                                var opsys = process.platform;
                                console.log(answersString);

                                dirString = answersString.split(" ");
                                console.log(dirString[3]);
                                if (opsys == "darwin") {
                                    opsys = "MacOS";
                                }
                                else if (opsys == "win32" || opsys == "win64") {
                                    child = exec(dir + 'git clone ' + dirString[3],
                                    { maxBuffer: 1024 * 1024 },
                                    function (error, stdout, stderr) {
                                        console.log(stdout);
                                        if (functions.testEmpty(stderr) !== 'empty-yes') {
                                            console.log('stderr: ' + stderr);
                                        }
                                        if (error !== null) {
                                            console.log('exec error: ' + error);

                                            // If it fails
                                            // functions.gitCloneFromUrlRetryOnFailure(dirString[1])
                                            if(dir === ''){
                                                dir = "."
                                            }
                                            functions.runCmdHandler(dir, "start cmd /k git clone " + dirString[3] + '.git');
                                            dir = '';
                                        }
                                    });
                                }
                                else if (opsys == "linux") {
                                    child = exec(dir + 'git clone ' + dirString[3],
                                    { maxBuffer: 1024 * 1024 },
                                    function (error, stdout, stderr) {
                                        console.log(stdout);
                                        if (functions.testEmpty(stderr) !== 'empty-yes') {
                                            console.log('stderr: ' + stderr);
                                        }
                                        if (error !== null) {
                                            console.log('exec error: ' + error);
                                        }
                                    });
                                }
                            });
                        })();
                    }
                });
            }
            if (answers.option == "Gitlab") {
                console.log(chalk.magentaBright('You can get a token from https://gitlab.com/-/profile/personal_access_tokens'));
                console.log(chalk.magentaBright('The token is 26 characters long and only shown when created.'));
                inquirer
                .prompt([
                    {
                        type: 'list',
                        name: 'option',
                        message: 'Choose an option:',
                        pageSize: '16',
                        choices: ['Public repos', 'Private repos'],
                    },
                ])
                .then(answers => {
                    if (answers.option == "Public repos") {
                        const questions = [
                            {
                                type: 'text',
                                name: 'username',
                                pageSize: '50',
                                message: 'What is your username?'
                            }
                        ];
                        (async () => {
                            const response = await prompts(questions);

                            let output = "";
                            let emptyLines = 0;
                            for (var i = 0; i < 10; i++) {
                                var execSyncOutput = execSync(dir + 'curl "https://gitlab.com/api/v4/users/'
                                    + response.username + '/projects?page=' + i + '&per_page=100" ');

                                execSyncOutput = functions.CleanString(execSyncOutput);
                                var objstdout = JSON.parse(execSyncOutput);

                                if (execSyncOutput.toString().slice(0, -1).length < 5) {
                                    emptyLines += 1;
                                }

                                try {

                                    for (var ii = 0; ii < objstdout.length; ii++) {

                                        output = output.concat(objstdout[ii].web_url + " ");
                                    }

                                }
                                catch (err) {

                                }
                            }
                            var gitURLList = output.split(" ");

                            var uniqueArray = gitURLList.filter(function (item, pos) {
                                return gitURLList.indexOf(item) == pos;
                            });
                            var filtered = uniqueArray.filter(function (el) {
                                if (el != "") {
                                    return el != null;
                                }
                            });
                            gitURLList = filtered;

                            for (var i = 0; i < gitURLList.length; i++) {
                                gitURLList[i] = i + ". " + gitURLList[i];
                            }

                            inquirer
                                .prompt([
                                    {
                                        type: 'list',
                                        name: 'option',
                                        message: 'Choose an option:',
                                        pageSize: '50',
                                        choices: gitURLList,
                                    },
                                ])
                                .then(answers => {
                                    if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanYes") {
                                        var answersString = answers.option;
                                        console.log(answersString);

                                        dirString = answersString.split(" ");
                                        console.log(dirString[0]);
                                        console.log(dirString[1]);
                                        if (opsys == "darwin") {
                                            opsys = "MacOS";
                                        }
                                        else if (opsys == "win32" || opsys == "win64") {
                                            console.log(answers.option);

                                        }
                                        else if (opsys == "linux") {
                                            // This is made to work on Ubuntu/Mint
                                            console.log(answers.option);

                                        }
                                    }

                                    if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanNo") {

                                        var answersString = answers.option;
                                        var opsys = process.platform;
                                        console.log(answersString);

                                        dirString = answersString.split(" ");

                                        if (opsys == "darwin") {
                                            opsys = "MacOS";
                                        }
                                        else if (opsys == "win32" || opsys == "win64") {

                                            child = exec(dir + 'git clone ' + dirString[1],
                                                { maxBuffer: 1024 * 1024 },
                                                function (error, stdout, stderr) {
                                                    console.log(stdout);
                                                    if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                        console.log('stderr: ' + stderr);
                                                    }
                                                    if (error !== null) {
                                                        console.log('exec error: ' + error);

                                                        // If it fails
                                                        // functions.gitCloneFromUrlRetryOnFailure(dirString[1])
                                                        if(dir === ''){
                                                            dir = "."
                                                        }
                                                        functions.runCmdHandler(dir, "start cmd /k git clone " + dirString[1] + '.git');
                                                        dir = '';
                                                    }
                                                });

                                        }
                                        else if (opsys == "linux") {
                                            child = exec(dir + 'git clone ' + dirString[1],
                                                { maxBuffer: 1024 * 1024 },
                                                function (error, stdout, stderr) {
                                                    console.log(stdout);
                                                    if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                        console.log('stderr: ' + stderr);
                                                    }
                                                    if (error !== null) {
                                                        console.log('exec error: ' + error);
                                                    }
                                                });
                                        }
                                    }
                                });
                        })();
                    }
                    if (answers.option == "Private repos") {
                        const questions = [
                            {
                                type: 'text',
                                name: 'username',
                                message: 'What is your username?'
                            }
                            ,
                            {
                                type: 'password',
                                name: 'private_token',
                                message: 'What is your private token?'
                            }
                        ];
                        (async () => {
                            const response = await prompts(questions);

                            let output = "";
                            let emptyLines = 0;
                            for (var i = 0; i < 10; i++) {
                                var execSyncOutput = execSync(dir + 'curl "https://gitlab.com/api/v4/users/'
                                    + response.username + '/projects?private_token=' + response.private_token
                                    + '&page=' + i + '&per_page=100" ');

                                execSyncOutput = functions.CleanString(execSyncOutput);
                                var objstdout = JSON.parse(execSyncOutput);
                                if (execSyncOutput.toString().slice(0, -1).length < 5) {
                                    emptyLines += 1;
                                }
                                try {

                                    for (var ii = 0; ii < objstdout.length; ii++) {

                                        output = output.concat(objstdout[ii].web_url + " ");
                                    }
                                }
                                catch (err) {
                                }
                            }
                            var gitURLList = output.split(" ");
                            var uniqueArray = gitURLList.filter(function (item, pos) {
                                return gitURLList.indexOf(item) == pos;
                            });

                            var filtered = uniqueArray.filter(function (el) {
                                if (el != "") {
                                    return el != null;
                                }
                            });

                            gitURLList = filtered;

                            for (var i = 0; i < gitURLList.length; i++) {
                                gitURLList[i] = i + ". " + gitURLList[i];
                            }

                            inquirer
                                .prompt([
                                    {
                                        type: 'list',
                                        name: 'option',
                                        message: 'Choose an option:',
                                        pageSize: '50',
                                        choices: gitURLList,
                                    },
                                ])
                                .then(answers => {
                                    if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanYes") {
                                        var answersString = answers.option;
                                        console.log(answersString);

                                        dirString = answersString.split(" ");
                                        console.log(dirString[0]);
                                        console.log(dirString[1]);
                                        if (opsys == "darwin") {
                                            opsys = "MacOS";
                                        }
                                        else if (opsys == "win32" || opsys == "win64") {
                                            console.log(answers.option);

                                        }
                                        else if (opsys == "linux") {
                                            // This is made to work on Ubuntu/Mint
                                            console.log(answers.option);

                                        }
                                    }

                                    if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanNo") {

                                        var answersString = answers.option;
                                        var opsys = process.platform;
                                        console.log(answersString);

                                        dirString = answersString.split(" ");

                                        if (opsys == "darwin") {
                                            opsys = "MacOS";
                                        }
                                        else if (opsys == "win32" || opsys == "win64") {

                                            child = exec(dir + 'git clone ' + dirString[1],
                                                { maxBuffer: 1024 * 1024 },
                                                function (error, stdout, stderr) {
                                                    console.log(stdout);
                                                    if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                        console.log('stderr: ' + stderr);
                                                    }
                                                    if (error !== null) {
                                                        console.log('exec error: ' + error);
                                                        
                                                        // If it fails
                                                        // functions.gitCloneFromUrlRetryOnFailure(dirString[1])
                                                        if(dir === ''){
                                                            dir = "."
                                                        }
                                                        functions.runCmdHandler(dir, "start cmd /k git clone " + dirString[1] + '.git');
                                                        dir = '';

                                                    }
                                                });
                                        }
                                        else if (opsys == "linux") {
                                            child = exec(dir + 'git clone ' + dirString[1],
                                                { maxBuffer: 1024 * 1024 },
                                                function (error, stdout, stderr) {
                                                    console.log(stdout);
                                                    if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                        console.log('stderr: ' + stderr);
                                                    }
                                                    if (error !== null) {
                                                        console.log('exec error: ' + error);
                                                    }
                                                });
                                        }
                                    }
                                });
                        })();
                    }
                });
            }
            if (answers.option == "Bitbucket") {
                inquirer
                    .prompt([
                        {
                            type: 'list',
                            name: 'option',
                            message: 'Choose an option:',
                            pageSize: '16',
                            choices: ['Public repos',
                            'Private repos - No scraping - using API, should work on theory but does not.',
                            'Private repos - Scraping - not using API, a bit slower',
                            ],
                        },
                    ])
                    .then(answers => {
                        if (answers.option == "Public repos") {
                            const questions = [
                                {
                                    type: 'text',
                                    name: 'username',
                                    pageSize: '50',
                                    message: 'What is your username?'
                                }
                            ];
                            (async () => {
                                const response = await prompts(questions);

                                let output = "";
                                let emptyLines = 0;
                                for (var i = 1; i < 11; i++) {
                                    var execSyncOutput = execSync(dir + 'curl "https://api.bitbucket.org/2.0/repositories/'
                                        + response.username + '?pagelen=100&limit=10000&page=' + [i] + '"');

                                    // console.log( execSyncOutput.toString().replace(/\n|\0|\t|\r|\f|\s/gim, ''));

                                    execSyncOutput = functions.CleanString(execSyncOutput.toString());

                                    if (execSyncOutput.toString().slice(0, -1).length < 5) {
                                        emptyLines += 1;
                                    }

                                    try {

                                        output = output + execSyncOutput.toString();
                                        if (i != 10) {
                                            output = output + ",";
                                        }
                                    }
                                    catch (err) {

                                    }

                                }

                                output = "[" + output + "]";

                                console.log(emptyLines);
                                console.log(emptyLines * 3 + 1);


                                var objstdout = JSON.parse(output.toString());
                                var gitURLList = [];

                                for (var iii = 0; iii < 10; iii++) {
                                    for (ii = 0; ii < objstdout[iii].values.length; ii++) {
                                        gitURLList.push(objstdout[iii].values[ii].links.self.href);
                                    }
                                }

                                for (var i = 0; i < gitURLList.length; i++) {
                                    gitURLList[i] = i + ". " + gitURLList[i];
                                }

                                inquirer
                                    .prompt([
                                        {
                                            type: 'list',
                                            name: 'option',
                                            message: 'Choose an option:',
                                            pageSize: '50',
                                            choices: gitURLList,
                                        },
                                    ])
                                    .then(answers => {
                                        if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanYes") {
                                            var answersString = answers.option;
                                            console.log(answersString);

                                            dirString = answersString.split(" ");
                                            console.log(dirString[0]);
                                            console.log(dirString[1]);
                                            if (opsys == "darwin") {
                                                opsys = "MacOS";
                                            }
                                            else if (opsys == "win32" || opsys == "win64") {
                                                console.log(answers.option);

                                            }
                                            else if (opsys == "linux") {
                                                // This is made to work on Ubuntu/Mint
                                                console.log(answers.option);

                                            }
                                        }

                                        if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanNo") {

                                            var answersString = answers.option;
                                            var opsys = process.platform;
                                            console.log(answersString);

                                            dirString = answersString.split(" ");
                                            // console.log(dirString[0]);
                                            dirStringCleanValue = dirString[1].replace("https://api.bitbucket.org/2.0/repositories/", "https://bitbucket.org/");

                                            if (opsys == "darwin") {
                                                opsys = "MacOS";
                                            }
                                            else if (opsys == "win32" || opsys == "win64") {

                                                child = exec(dir + 'git clone ' + dirStringCleanValue,
                                                    { maxBuffer: 1024 * 1024 },
                                                    function (error, stdout, stderr) {
                                                        console.log(stdout);
                                                        if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                            console.log('stderr: ' + stderr);
                                                        }
                                                        if (error !== null) {
                                                            console.log('exec error: ' + error);

                                                            // If it fails
                                                            // functions.gitCloneFromUrlRetryOnFailure(dirStringCleanValue)
                                                            if(dir === ''){
                                                                dir = "."
                                                            }
                                                            functions.runCmdHandler(dir, "start cmd /k git clone " + dirStringCleanValue + '.git');
                                                            dir = '';

                                                        }
                                                    });

                                            }
                                            else if (opsys == "linux") {
                                                child = exec(dir + 'git clone ' + dirStringCleanValue,
                                                    { maxBuffer: 1024 * 1024 },
                                                    function (error, stdout, stderr) {
                                                        console.log(stdout);
                                                        if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                            console.log('stderr: ' + stderr);
                                                        }
                                                        if (error !== null) {
                                                            console.log('exec error: ' + error);
                                                        }
                                                    });

                                            }

                                        }
                                    });
                            })();
                        }
                        if (answers.option == "Private repos - No scraping - using API, should work on theory but does not.") {
                            const questions = [
                                {
                                    type: 'text',
                                    name: 'username',
                                    message: 'What is your username?'
                                }
                                ,
                                {
                                    type: 'password',
                                    name: 'password',
                                    message: 'What is your password?'
                                }

                            ];
                            (async () => {
                                const response = await prompts(questions);

                                let output = "";
                                let emptyLines = 0;
                                for (var i = 1; i < 11; i++) {
                                    var execSyncOutput = execSync(dir + 'curl "https://api.bitbucket.org/2.0/repositories/' + response.username + '?pagelen=100&limit=10000&page='
                                        + [i] + '"' + ' -u ' + '"' + response.username + ':' + response.password + '"');

                                    execSyncOutput = functions.CleanString(execSyncOutput.toString());

                                    if (execSyncOutput.toString().slice(0, -1).length < 5) {
                                        emptyLines += 1;
                                    }

                                    try {

                                        output = output + execSyncOutput.toString();
                                        if (i != 10) {
                                            output = output + ",";
                                        }
                                    }
                                    catch (err) {

                                    }

                                }

                                output = "[" + output + "]";

                                console.log(emptyLines);
                                console.log(emptyLines * 3 + 1);


                                var objstdout = JSON.parse(output.toString());
                                var gitURLList = [];


                                for (var iii = 0; iii < 10; iii++) {

                                    for (ii = 0; ii < objstdout[iii].values.length; ii++) {

                                        gitURLList.push(objstdout[iii].values[ii].links.self.href);
                                        // console.log(objstdout.values[ii]);
                                    }
                                }

                                for (var i = 0; i < gitURLList.length; i++) {
                                    gitURLList[i] = i + ". " + gitURLList[i];
                                }

                                inquirer
                                    .prompt([
                                        {
                                            type: 'list',
                                            name: 'option',
                                            message: 'Choose an option:',
                                            pageSize: '50',
                                            choices: gitURLList,
                                        },
                                    ])
                                    .then(answers => {
                                        if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanYes") {
                                            var answersString = answers.option;
                                            console.log(answersString);

                                            dirString = answersString.split(" ");
                                            console.log(dirString[0]);
                                            console.log(dirString[1]);
                                            if (opsys == "darwin") {
                                                opsys = "MacOS";
                                            }
                                            else if (opsys == "win32" || opsys == "win64") {
                                                console.log(answers.option);

                                            }
                                            else if (opsys == "linux") {
                                                // This is made to work on Ubuntu/Mint
                                                console.log(answers.option);
                                            }
                                        }

                                        if (PseudoBooleanOperationControl == "EndHerePseudo-BooleanNo") {
                                            var answersString = answers.option;
                                            var opsys = process.platform;
                                            console.log(answersString);

                                            dirString = answersString.split(" ");
                                            dirStringCleanValue = dirString[1].replace("https://api.bitbucket.org/2.0/repositories/", "https://bitbucket.org/");

                                            if (opsys == "darwin") {
                                                opsys = "MacOS";
                                            }
                                            else if (opsys == "win32" || opsys == "win64") {
                                                child = exec(dir + 'git clone ' + dirStringCleanValue,
                                                    { maxBuffer: 1024 * 1024 },
                                                    function (error, stdout, stderr) {
                                                        console.log(stdout);
                                                        if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                            console.log('stderr: ' + stderr);
                                                        }
                                                        if (error !== null) {
                                                            console.log('exec error: ' + error);

                                                            // If it fails
                                                            // functions.gitCloneFromUrlRetryOnFailure(dirStringCleanValue)
                                                            if(dir === ''){
                                                                dir = "."
                                                            }
                                                            functions.runCmdHandler(dir, "start cmd /k git clone " + dirStringCleanValue + '.git');
                                                            dir = '';
                                                        }
                                                    });

                                            }
                                            else if (opsys == "linux") {
                                                child = exec(dir + 'git clone ' + dirStringCleanValue,
                                                    { maxBuffer: 1024 * 1024 },
                                                    function (error, stdout, stderr) {
                                                        console.log(stdout);
                                                        if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                            console.log('stderr: ' + stderr);
                                                        }
                                                        if (error !== null) {
                                                            console.log('exec error: ' + error);
                                                        }
                                                    });

                                            }
                                        }
                                    });
                            })();
                        }
                        if (answers.option == "Private repos - Scraping - not using API, a bit slower") {
                            const questions = [
                                {
                                    type: 'text',
                                    name: 'emailUsedForLogin',
                                    message: 'What is your username/email used for login?'
                                }
                                ,
                                {
                                    type: 'password',
                                    name: 'password',
                                    message: 'What is your password?'
                                }
                                ,
                                {
                                    type: 'text',
                                    name: 'usernameOfDistinationDir',
                                    message: 'What is the username of the account?'
                                }
                            ];
                            (async () => {
                                const response = await prompts(questions);
                                console.log(chalk.magentaBright('Please wait a bit for the scraping to finish'));
                                populateArray(response.emailUsedForLogin,response.password,response.usernameOfDistinationDir)
                                .then((resultArray) => {
                                    inquirer
                                    .prompt([
                                        {
                                            type: 'list',
                                            name: 'option',
                                            message: 'Choose an option:',
                                            pageSize: '50',
                                            choices: resultArray.flat(),
                                        },
                                    ])
                                    .then(answers => {
                                        var answersString = answers.option;
                                        var opsys = process.platform;
                                        let dirString = "https://bitbucket.org/" + response.usernameOfDistinationDir + "/" + answersString;
                                        if (opsys == "darwin") {
                                            opsys = "MacOS";
                                        }
                                        else if (opsys == "win32" || opsys == "win64") {
                                            child = exec(dir + 'git clone ' + dirString + '.git',
                                            { maxBuffer: 1024 * 1024 },
                                            function (error, stdout, stderr) {
                                                console.log(stdout);
                                                if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                    console.log('stderr: ' + stderr);
                                                }
                                                if (error !== null) {
                                                    console.log('exec error: ' + error);
                                                    if(dir === ''){
                                                        dir = "."
                                                    }
                                                    functions.runCmdHandler(dir, "start cmd /k git clone " + dirString + '.git');
                                                    dir = '';
                                                }
                                            });
                                        }
                                        else if (opsys == "linux") {
                                            child = exec(dir + 'git clone ' + dirString + '.git',
                                            { maxBuffer: 1024 * 1024 },
                                            function (error, stdout, stderr) {
                                                console.log(stdout);
                                                if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                    console.log('stderr: ' + stderr);
                                                }
                                                if (error !== null) {
                                                    console.log('exec error: ' + error);
                                                }
                                            });
                                        }
                                    });
                                })
                                .catch((error) => {
                                    console.error('oh no');
                                    console.error(error);
                                });
                            })();
                        }
                    });
            }
            if (answers.option == "Gitea") {
                inquirer
                .prompt([
                    {
                        type: 'list',
                        name: 'option',
                        message: 'Choose an option:',
                        pageSize: '16',
                        choices: ['Public repos',
                        'Private repos',
                        ],
                    },
                ])
                .then(answers => {
                    if (answers.option == "Public repos") {
                        const questions = [
                            {
                                type: 'text',
                                name: 'accountName',
                                pageSize: '50',
                                message: 'What is the account name?'
                            }
                        ];
                        (async () => {
                            const response = await prompts(questions);

                            exec('curl -X GET https://gitea.com/api/v1/users/' + response.accountName + '/repos?',
                            { maxBuffer: 1024 * 1024 },
                            function (error, stdout, stderr) {
                                if (error) {
                                    console.error(`Error: ${error}`);
                                    return;
                                }
                                // Command executed successfully
                                console.log(`stdout: ${stdout}`);
                                // console.error(`stderr: ${stderr}`);

                                var objstdout = JSON.parse(stdout.toString());
                                var gitURLList = [];

                                for (ii = 0; ii < objstdout.length; ii++) {
                                    gitURLList.push(objstdout[ii].name);
                                }
                                inquirer
                                    .prompt([
                                        {
                                            type: 'list',
                                            name: 'option',
                                            message: 'Choose an option:',
                                            pageSize: '50',
                                            choices: gitURLList,
                                        },
                                    ])
                                    .then(answers => {
                                        var answersString = answers.option;
                                        var opsys = process.platform;
                                        console.log(answersString);

                                        dirString = 'https://gitea.com/'+response.accountName+'/'+answersString+'.git';

                                        if (opsys == "darwin") {
                                            opsys = "MacOS";
                                        }
                                        else if (opsys == "win32" || opsys == "win64") {
                                            child = exec(dir + 'git clone ' + dirString,
                                                { maxBuffer: 1024 * 1024 },
                                                function (error, stdout, stderr) {
                                                    console.log(stdout);
                                                    if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                        console.log('stderr: ' + stderr);
                                                    }
                                                    if (error !== null) {
                                                        console.log('exec error: ' + error);

                                                        // If it fails
                                                        // functions.gitCloneFromUrlRetryOnFailure(dirStringCleanValue)
                                                        if(dir === ''){
                                                            dir = "."
                                                        }
                                                        functions.runCmdHandler(dir, "start cmd /k git clone " + dirString + '.git');
                                                        dir = '';
                                                    }
                                                });

                                        }
                                        else if (opsys == "linux") {
                                            child = exec(dir + 'git clone ' + dirString,
                                                { maxBuffer: 1024 * 1024 },
                                                function (error, stdout, stderr) {
                                                    console.log(stdout);
                                                    if (functions.testEmpty(stderr) !== 'empty-yes') {
                                                        console.log('stderr: ' + stderr);
                                                    }
                                                    if (error !== null) {
                                                        console.log('exec error: ' + error);
                                                    }
                                                });
                                        }
                                        
                                    });
                            });
                        })();
                    }
                    if (answers.option == "Private repos") {
                        const questions = [
                            {
                                type: 'text',
                                name: 'username',
                                pageSize: '50',
                                message: 'What is your username?'
                            },
                            {
                                type: 'password',
                                name: 'password',
                                pageSize: '50',
                                message: 'What is your password?'
                            },
                            {
                                type: 'text',
                                name: 'accountName',
                                pageSize: '50',
                                message: 'What is the account name?'
                            }
                        ];
                        (async () => {
                            const response = await prompts(questions);

                            exec('curl -X GET https://'+ response.username+':'+ response.password+'@gitea.com/api/v1/users/' + response.accountName + '/repos?',
                            { maxBuffer: 1024 * 1024 },
                            function (error, stdout, stderr) {
                                if (error) {
                                    console.error(`Error: ${error}`);
                                    return;
                                }
                                // Command executed successfully
                                // console.log(`stdout: ${stdout}`);
                                // console.error(`stderr: ${stderr}`);

                                var objstdout = JSON.parse(stdout.toString());
                                var gitURLList = [];

                                for (ii = 0; ii < objstdout.length; ii++) {
                                    gitURLList.push(objstdout[ii].name);
                                }

                                console.log(gitURLList);
                            });
                        })();
                    }
                });
            }
        });
    },
    Init: (hashedDir) => {
        inquirer
        .prompt([
            {
                type: 'list',
                name: 'option',
                message: 'Choose an option:',
                pageSize: '50',
                choices: [chalk.green('Creating a new repository on the command line'), 
                    chalk.green('Pushing an existing repository from the command line'),
                ],
            },
        ])
        .then(answers => {
            const questions = [
                {
                    type: 'text',
                    name: 'remoteOriginUrl',
                    message: 'What is the remote origin url? Example of a remote origin: \"https://gitea.com/User/testRepo.git\"'
                }
            ];
            (async () => {
                const response = await prompts(questions);
                if (answers.option == chalk.green('Creating a new repository on the command line')) {
                    var opsys = process.platform;
                    if (opsys == "darwin") {
                        opsys = "MacOS";
                    }
                    else if (opsys == "win32" || opsys == "win64") {
                        exec('cd ' + hashedDir + ' && echo.> README.md',
                        { maxBuffer: 1024 * 1024 },
                        function (error, stdout, stderr) {
                            if (error) {
                                console.error(`Error: ${error}`);
                                return;
                            }
                            // Command executed successfully
                            console.log(`stdout: ${stdout}`);
                            console.error(`stderr: ${stderr}`);

                            exec('cd ' + hashedDir + ' && git init',
                            { maxBuffer: 1024 * 1024 },
                            function (error, stdout, stderr) {
                                if (error) {
                                    console.error(`Error: ${error}`);
                                    return;
                                }
                                // Command executed successfully
                                console.log(`stdout: ${stdout}`);
                                console.error(`stderr: ${stderr}`);
    
                                exec('cd ' + hashedDir + ' && git checkout -b main',
                                { maxBuffer: 1024 * 1024 },
                                function (error, stdout, stderr) {
                                    if (error) {
                                        console.error(`Error: ${error}`);
                                        return;
                                    }
                                    // Command executed successfully
                                    console.log(`stdout: ${stdout}`);
                                    console.error(`stderr: ${stderr}`);
        
                                    exec('cd ' + hashedDir + ' && git add README.md',
                                    { maxBuffer: 1024 * 1024 },
                                    function (error, stdout, stderr) {
                                        if (error) {
                                            console.error(`Error: ${error}`);
                                            return;
                                        }
                                        // Command executed successfully
                                        console.log(`stdout: ${stdout}`);
                                        console.error(`stderr: ${stderr}`);
            
                                        exec('cd ' + hashedDir + ' && git commit -m "first commit"',
                                        { maxBuffer: 1024 * 1024 },
                                        function (error, stdout, stderr) {
                                            if (error) {
                                                console.error(`Error: ${error}`);
                                                return;
                                            }
                                            // Command executed successfully
                                            console.log(`stdout: ${stdout}`);
                                            console.error(`stderr: ${stderr}`);
                
                                            exec('cd ' + hashedDir + ' && git remote add origin ' + response.remoteOriginUrl,
                                            { maxBuffer: 1024 * 1024 },
                                            function (error, stdout, stderr) {
                                                if (error) {
                                                    console.error(`Error: ${error}`);
                                                    return;
                                                }
                                                // Command executed successfully
                                                console.log(`stdout: ${stdout}`);
                                                console.error(`stderr: ${stderr}`);
                                                
                                                exec('cd ' + hashedDir + ' && git push -u origin main',
                                                { maxBuffer: 1024 * 1024 },
                                                function (error, stdout, stderr) {
                                                    if (error) {
                                                        console.error(`Error: ${error}`);
                                                        return;
                                                    }
                                                    // Command executed successfully
                                                    console.log(`stdout: ${stdout}`);
                                                    console.error(`stderr: ${stderr}`);    
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    }
                    else if (opsys == "linux") {
                        exec('cd ' + hashedDir + ' && touch README.md',
                        { maxBuffer: 1024 * 1024 },
                        function (error, stdout, stderr) {
                            if (error) {
                                console.error(`Error: ${error}`);
                                return;
                            }
                            // Command executed successfully
                            console.log(`stdout: ${stdout}`);
                            console.error(`stderr: ${stderr}`);

                            exec('cd ' + hashedDir + ' && git init',
                            { maxBuffer: 1024 * 1024 },
                            function (error, stdout, stderr) {
                                if (error) {
                                    console.error(`Error: ${error}`);
                                    return;
                                }
                                // Command executed successfully
                                console.log(`stdout: ${stdout}`);
                                console.error(`stderr: ${stderr}`);
    
                                exec('cd ' + hashedDir + ' && git checkout -b main',
                                { maxBuffer: 1024 * 1024 },
                                function (error, stdout, stderr) {
                                    if (error) {
                                        console.error(`Error: ${error}`);
                                        return;
                                    }
                                    // Command executed successfully
                                    console.log(`stdout: ${stdout}`);
                                    console.error(`stderr: ${stderr}`);
        
                                    exec('cd ' + hashedDir + ' && git add README.md',
                                    { maxBuffer: 1024 * 1024 },
                                    function (error, stdout, stderr) {
                                        if (error) {
                                            console.error(`Error: ${error}`);
                                            return;
                                        }
                                        // Command executed successfully
                                        console.log(`stdout: ${stdout}`);
                                        console.error(`stderr: ${stderr}`);
            
                                        exec('cd ' + hashedDir + ' && git commit -m "first commit"',
                                        { maxBuffer: 1024 * 1024 },
                                        function (error, stdout, stderr) {
                                            if (error) {
                                                console.error(`Error: ${error}`);
                                                return;
                                            }
                                            // Command executed successfully
                                            console.log(`stdout: ${stdout}`);
                                            console.error(`stderr: ${stderr}`);
                
                                            exec('cd ' + hashedDir + ' && git remote add origin ' + response.remoteOriginUrl,
                                            { maxBuffer: 1024 * 1024 },
                                            function (error, stdout, stderr) {
                                                if (error) {
                                                    console.error(`Error: ${error}`);
                                                    return;
                                                }
                                                // Command executed successfully
                                                console.log(`stdout: ${stdout}`);
                                                console.error(`stderr: ${stderr}`);
                                                
                                                exec('cd ' + hashedDir + ' && git push -u origin main',
                                                { maxBuffer: 1024 * 1024 },
                                                function (error, stdout, stderr) {
                                                    if (error) {
                                                        console.error(`Error: ${error}`);
                                                        return;
                                                    }
                                                    // Command executed successfully
                                                    console.log(`stdout: ${stdout}`);
                                                    console.error(`stderr: ${stderr}`);    
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    }
                }
                if (answers.option == chalk.green('Pushing an existing repository from the command line')) {
                    exec('cd ' + hashedDir + ' && git remote add origin ' + response.remoteOriginUrl,
                    { maxBuffer: 1024 * 1024 },
                    function (error, stdout, stderr) {
                        if (error) {
                            console.error(`Error: ${error}`);
                            return;
                        }
                        // Command executed successfully
                        console.log(`stdout: ${stdout}`);
                        console.error(`stderr: ${stderr}`);
                        
                        exec('cd ' + hashedDir + ' && git push -u origin main',
                        { maxBuffer: 1024 * 1024 },
                        function (error, stdout, stderr) {
                            if (error) {
                                console.error(`Error: ${error}`);
                                return;
                            }
                            // Command executed successfully
                            console.log(`stdout: ${stdout}`);
                            console.error(`stderr: ${stderr}`);    
                        });
                    });
                }
            })();
        });
    },
    ApiTokenListOptions: (hashedDir) => {
        inquirer
        .prompt([
            {
                type: 'list',
                name: 'option',
                message: 'Choose an option:',
                pageSize: '16',
                choices: ['Github' ,
                'Gitlab', 
                'Bitbucket', 
                'Gitea'],
            },
        ])
        .then(answers => {
            if (answers.option == "Github") {
            }
            if (answers.option == "Gitlab") {
            }
            if (answers.option == "Bitbucket") {
            }
            if (answers.option == "Gitea") {
                const questions = [
                    {
                        type: 'text',
                        name: 'username',
                        message: 'What is your username?'
                    },
                    {
                        type: 'password',
                        name: 'password',
                        message: 'What is your password?'
                    }
                ];
                (async () => {
                    const response = await prompts(questions);

                    if (opsys == "darwin") {
                        opsys = "MacOS";
                    }
                    else if (opsys == "win32" || opsys == "win64") {
                        let command = 'curl https://'+response.username+':'+response.password+'@gitea.com/api/v1/users/'+response.username+'/tokens'
                        exec(command,
                        { maxBuffer: 1024 * 1024 },
                        function (error, stdout, stderr) {
                            if (error) {
                                console.error(`command: ` + command);
                                console.error(`Error: ${error}`);
                                return;
                            }

                            var output = JSON.parse(stdout.toString());
                            inquirer
                            .prompt([
                                {
                                    type: 'list',
                                    name: 'option',
                                    message: 'Choose an option:',
                                    pageSize: '50',
                                    choices: output,
                                },
                            ])
                            .then(answers => {
                                var answersString = answers.option;
                                console.log('The token chosen is: ' + answersString);
                            });
                        });
                    }
                    else if (opsys == "linux") {
                        
                    }
                })();
            }
        });
    },
    ApiTokenCreateOptions: (hashedDir) => {
        inquirer
        .prompt([
            {
                type: 'list',
                name: 'option',
                message: 'Choose an option:',
                pageSize: '16',
                choices: ['Github' ,
                'Gitlab', 
                'Bitbucket', 
                'Gitea'],
            },
        ])
        .then(answers => {
            if (answers.option == "Github") {
            }
            if (answers.option == "Gitlab") {
            }
            if (answers.option == "Bitbucket") {
            }
            if (answers.option == "Gitea") {
                const questions = [
                    {
                        type: 'text',
                        name: 'username',
                        message: 'What is your username?'
                    },
                    {
                        type: 'password',
                        name: 'password',
                        message: 'What is your password?'
                    },
                    {
                        type: 'text',
                        name: 'tokenName',
                        message: 'Choose a name for the token?'
                    }

                ];
                (async () => {
                    const response = await prompts(questions);

                    if (opsys == "darwin") {
                        opsys = "MacOS";
                    }
                    else if (opsys == "win32" || opsys == "win64") {
                        let command = 'curl -H "Content-Type: application/json" -d '
                        + '"'+'{'+'\\"name\\"'
                        +':'+'\\"'
                        +response.tokenName
                        +'\\"'
                        +'}'+'" ' 
                        + 'https://'+response.username+':'+response.password+'@gitea.com/api/v1/users/'+response.username+'/tokens'
                        exec(command,
                        { maxBuffer: 1024 * 1024 },
                        function (error, stdout, stderr) {
                            if (error) {
                                console.error(`command: ` + command);
                                console.error(`Error: ${error}`);
                                return;
                            }
                            // Command executed successfully
                            console.log(`command: ` + command);
                            console.log(`stdout: ${stdout}`);
                            console.error(`stderr: ${stderr}`);    
                        });
                    }
                    else if (opsys == "linux") {
                        
                    }
                })();
            }
        });
    },
    ApiTokenDeleteOptions: (hashedDir) => {
        inquirer
        .prompt([
            {
                type: 'list',
                name: 'option',
                message: 'Choose an option:',
                pageSize: '16',
                choices: ['Github' ,
                'Gitlab', 
                'Bitbucket', 
                'Gitea'],
            },
        ])
        .then(answers => {
            if (answers.option == "Github") {
            }
            if (answers.option == "Gitlab") {
            }
            if (answers.option == "Bitbucket") {
            }
            if (answers.option == "Gitea") {
                const questions = [
                    {
                        type: 'text',
                        name: 'username',
                        message: 'What is your username?'
                    },
                    {
                        type: 'password',
                        name: 'password',
                        message: 'What is your password?'
                    }
                ];
                (async () => {
                    const response = await prompts(questions);

                    if (opsys == "darwin") {
                        opsys = "MacOS";
                    }
                    else if (opsys == "win32" || opsys == "win64") {
                        let command = 'curl https://'+response.username+':'+response.password+'@gitea.com/api/v1/users/'+response.username+'/tokens'
                        exec(command,
                        { maxBuffer: 1024 * 1024 },
                        function (error, stdout, stderr) {
                            if (error) {
                                console.error(`command: ` + command);
                                console.error(`Error: ${error}`);
                                return;
                            }

                            var output = JSON.parse(stdout.toString());
                            inquirer
                            .prompt([
                                {
                                    type: 'list',
                                    name: 'option',
                                    message: 'Choose an option:',
                                    pageSize: '50',
                                    choices: output,
                                },
                            ])
                            .then(answers => {
                                var answersString = answers.option;

                                let command2 = 'curl -X DELETE  https://'+response.username+':'+response.password+'@gitea.com/api/v1/users/'+response.username+'/tokens/'+answersString;
                                exec(command2,
                                { maxBuffer: 1024 * 1024 },
                                function (error, stdout, stderr) {
                                    if (error) {
                                        console.error(`command: ` + command2);
                                        console.error(`Error: ${error}`);
                                        return;
                                    }
                                });

                            });
                        });
                    }
                    else if (opsys == "linux") {
                        
                    }
                })();
            }
        });
    },
    SETTINGS_Credential_Manager: (hashedDir) => {
        if (opsys == "darwin") {
            opsys = "MacOS";
        }
        else if (opsys == "win32" || opsys == "win64") {
            console.log(hashedDir);

            var dir="";
            if (hashedDir.localeCompare('') === 0) {
                dir = '';
            }
            else {
                dir = 'cd ' + hashedDir + ' && ';
            }

            child = exec(dir + 'rundll32.exe keymgr.dll, KRShowKeyMgr',
            { maxBuffer: 1024 * 1024 },
            function (error, stdout, stderr) {
                console.log(stdout);
                if (functions.testEmpty(stderr) !== 'empty-yes') {
                    console.log('stderr: ' + stderr);
                }
                if (error !== null) {
                    console.log('exec error: ' + error);
                }
            });

        }
        else if (opsys == "linux") {
            console.log(hashedDir);
        }
    }
};