const inquirer = require('inquirer');
var fs = require('fs');
var exec = require('child_process').exec, child;
const prompts = require('prompts');
const { execSync } = require('child_process');

var figlet = require('figlet');
const chalk = require('chalk');

var Spinner = require('cli-spinner').Spinner;
var spawn = require('child_process').spawn;

const gradient = require('gradient-string');
const CFonts = require('cfonts');
const functions = require('./functions');

const optionsList = require('./optionsList');

const BRANCHINGmakenewbranches = require('./BRANCHING-make-new-branches');
const BRANCHINGShowbranches = require('./BRANCHING-Show-branches');
const BRANCHINGSwitchbranches = require('./BRANCHING-Switch-branches');
const BRANCHINGRemovebranch = require('./BRANCHING-Remove-branch');
const BRANCHINGMergebranch = require('./BRANCHING-Merge-branch');
const REVERTHeadAndLastCommitSoft = require('./REVERTHeadAndLastCommitSoft');
const REVERTHeadAndLastCommitHard = require('./REVERTHeadAndLastCommitHard');
const REVERTReturnthelastcommit = require('./REVERTReturnthelastcommit');
const INFOVisualisegit = require('./INFO-Visualise-git');
const INFOGotoorigin = require('./INFO-Go-to-origin');
const INFOGetlistofrepos = require('./INFO-Get-list-of-repos');
const INFOGitlog = require('./INFO-git-log');
const INFOGitReflog = require('./INFO-GitReflog');
const INFOGitshow = require('./INFO-git-show');
const INFOGitdiff = require('./INFO-git-diff');
const INFOGitStatus = require('./INFO-git-status');
const INFOGitShowAllRemotes = require('./INFO-git-remote-v');
const TaggingListAllTags = require('./TaggingListAllTags');
const TaggingShow = require('./TaggingShow');
const TaggingCreateLightweightTags = require('./TaggingCreateLightweightTags');
const TaggingCreateAnnotatedTags = require('./TaggingCreateAnnotatedTags');
const TaggingPusAllhTagsToRemote = require('./TaggingPusAllhTagsToRemote');
const TaggingDeleteTags = require('./TaggingDeleteTags');
const MANAGEMENTRegisterrepo = require('./MANAGEMENT-Register-repo');
const MANAGEMENTOpenlocalrepo = require('./MANAGEMENT-Open-local-repo');
const UTILITYgitconfigcoreautocrlftrueforWindows = require('./UTILITY-git-config-core.autocrlf-true-(-for-Windows-)');
const HELPShowinfographic = require('./HELP-Show-infographic');
const HELPShowohshitgit = require('./HELP-Show-ohshitgit.com');
const SETTINGSEnableordisablestartupdatafields = require('./SETTINGS-Enable-or-disable-startup-data-fields');
const SETTINGSEnableordisableStatusVisualisationTypes = require('./SETTINGS-Enable-or-disable-Status-Visualisation-Types');
const SETTINGSEnableordisableMiscellaneousSettings = require('./SETTINGS-MiscellaneousSettings');
const BRANCHINGRemoveremotebranch = require('./BRANCHING-Remove-remote-branch');

const functionsForReadingAndWritingToIniConfigFiles = require('./StartupScreen/functionsForReadingAndWritingToIniConfigFiles');
const iniFile = './StartupScreen/easyGitConfig.ini';
var topNumberOfLinesToDisplay = 15;
var truncation = "'Yes'";

topNumberOfLinesToDisplay=functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('topNumberOfLinesToDisplay',iniFile);
truncation=functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('truncation',iniFile);
topNumberOfLinesToDisplay = parseInt(topNumberOfLinesToDisplay);

var hashedDir = functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('hashed_current_dir',iniFile);

var currentInitializedStatus = false;
var hashedInitializedStatus = false;
var directoryChosen;

message: console.log(chalk.black.green ('Welcome, enjoy your coding ;)'));

setTimeout(() => {
    Promise.resolve(currentDirMessage()).then((value) => {
        if(value.localeCompare('completed') === 0){
            setTimeout(() => {
                Promise.resolve(hashedDirMessage()).then((value) => {
                    if(value.localeCompare('completed') === 0){
                        dialogSelector();
                    }
                });
            }, 150);
        }
    });
}, 150);
    

function currentDirMessage(){
    return new Promise((resolve) => {
        exec('cd ' + __dirname + ' && git rev-parse --is-inside-work-tree', (err, stdout, stderr) => {
            if (err) {
                message: console.log(chalk.black.blue ('Current directory: | ' + 'git is not initialized' + " | " + __dirname));
            }
            if(stdout){
                message: console.log(chalk.black.blue ('Current directory: | ' + 'git is initialized' + " | " + __dirname));
                currentInitializedStatus = true;
            }
        });
        resolve('completed');
    });
}
function hashedDirMessage(){
    return new Promise((resolve) => {
        exec('cd ' + hashedDir + ' && git rev-parse --is-inside-work-tree', (err, stdout, stderr) => {
            if (err) {
                message: console.log(chalk.black.blue ('Hashed directory: | ' + 'git is not initialized' + " | " + hashedDir.replaceAll("/", "\\")));
            }
            if(stdout){
                message: console.log(chalk.black.blue ('Hashed directory: | ' + 'git is initialized' + " | " + hashedDir.replaceAll("/", "\\")));
                hashedInitializedStatus = true;
            }
            if(hashedDir.localeCompare('the attribute does not exists.') == 0) {
                message: console.log(chalk.black.red ('There is no hashed directory'));
                choicesArray = [
                        chalk.cyan('Use hashed directory'),
                        chalk.cyan('Choose another directory'), 
                    ]
            }
            else{
                choicesArray = [
                    chalk.cyan('Use current directory'),
                    chalk.cyan('Use hashed directory'),
                    chalk.cyan('Choose another directory'), 
                ]
            } 
            resolve('completed');
        });
    });
}
function dialogSelector(){
    message: console.log(chalk.black.bgBlackBright (' Choose an option:                                                                                                      '));
    inquirer
        .prompt([
        {
            type: 'list',
            name: 'option',
            pageSize: '3',
            choices: choicesArray,
        },
    ])
    .then(answers => {

        if (answers.option == chalk.cyan('Use current directory')) {
            hashedDir = '';
            directoryChosen = 'current';
            launcherMain(hashedDir);
        }

        if (answers.option == chalk.cyan('Use hashed directory')) {
            directoryChosen = 'hashed';
            launcherMain(hashedDir);
        }

        if (answers.option == chalk.cyan('Choose another directory')) {
            
            const dialog = require('node-file-dialog')
            const config = {
                type: 'directory'
            };
            dialog(config)
            .then(dir => SelectNewDir(dir))
            .catch(err => console.log(err))
        }
    });
}

function launcherMain(hashedDir){
    let chain0 = Promise.resolve(AppVersion())
    let chain1 = Promise.resolve(gitStatusTypeSimpleFilesTrackingCallingFunction(hashedDir))
    let chain2 = Promise.resolve(gitStatusTypeOnelineGraphDecorateColorShortstatCallingFunction(hashedDir))
    let chain3 = Promise.resolve(gitLogAbbreviatedCommitsInAshortstatOneLineGraph())
    let chain4 = Promise.resolve(ShowCommitsPerDayInANiceFormattedWay())

    setTimeout(() => {
        chain0.then((value) => {
            if(value.localeCompare('completed') === 0){
                setTimeout(() => {
                    chain1.then((value) => {
                        if(value.localeCompare('completed') === 0){
                            setTimeout(() => {
                                chain2.then((value) => {
                                    if(value.localeCompare('completed') === 0){
                                        setTimeout(() => {
                                            chain3.then((value) => {
                                                if(value.localeCompare('completed') === 0){
                                                    setTimeout(() => {
                                                        chain4.then((value) => {
                                                            if(value.localeCompare('completed') === 0){
                                                                main();
                                                            }
                                                        });
                                                    }, 150);
                                                }
                                            });
                                        }, 150);
                                    }
                                });
                            }, 150);
                        }
                    });
                }, 150);
            }
        });
    }, 150);
}

function AppVersion(){
    return new Promise((resolve) => {
        if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('AppVersion',iniFile).localeCompare('Yes') === 0){
            console.log(gradient.summer(' App Version - 1.5                                                                                                                                                           '));
        }
         resolve('completed');
    });
}
function gitBranch(){
    return new Promise((resolve) => {
        if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('gitBranches',iniFile).localeCompare('Yes') === 0){
            console.log(chalk.black.bgGreen(' git branches -                                                                                                                                                                \n'));
            child = exec(' git branch -a --color',
                { maxBuffer: 512 * 512 },
                    function (error, stdout, stderr) {
                        if(stdout.length == 0){
                            console.log("no git branches found");
                        }
                        else{
                            console.log(stdout);
                        }
                         resolve('completed');
                }
            );
        }
        if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('gitBranches',iniFile).localeCompare('No') === 0){
             resolve('completed');
        }
    });
}
function localRepos(){
    return new Promise((resolve) => {
        if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('localRepos',iniFile).localeCompare('Yes') === 0){
            // if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('localRepos',iniFile) === "'Yes'"){
            
            console.log(chalk.black.bgCyan(' List of local repos on this PC, registered in easyGit                                                                                                                         \n'));

            var opsys = process.platform;
            // console.log(chalk.black.bgCyan(opsys));
            if (opsys == "darwin") {
                opsys = "MacOS";
                console.log(chalk.black.bgCyan(' Mac is not really supported because I have no easy way to test it. '));
                 resolve('completed');
            }
            else if (opsys == "win32" || opsys == "win64") {
                // WinUserName = functions.getUserHome();
                var dir = './GitRepoList';
    
                var FileDir = dir + "/List.txt";
                if (fs.existsSync(FileDir)) {
                    FileText = fs.readFileSync(dir + "/List.txt");
                    var fileSet = functions.uniq(FileText.toString().split(/\r?\n/).clean(''));

                    console.log(chalk.blue("Repository's local directory") + "                                        |" + chalk.cyan(" Remote hosting website") + "                                    |"+chalk.green(" Project name")                        );

                    for (var i=0;i<fileSet.length;i++){
                        console.log(chalk.blue(fileSet[i].replace(/,/g,'\n').split(' ')[0].padEnd(70, ' ')) 
                        + chalk.cyan(fileSet[i].replace(/,/g,'\n').split(' ')[1].replace('https://','').replace('https:////','').replace('https////','').replace('https///','').split('/')[0].padEnd(60, ' ')) 
                        + chalk.green(fileSet[i].replace(/,/g,'\n').split(' ')[1].replace('https://','').replace('https:////','').replace('https////','').replace('https///','').split('/')[fileSet[i].replace(/,/g,'\n').split(' ').length]));
                    }
    
                    console.log("");
                     resolve('completed');

                }
                else {
                    console.log("ERROR: no local repos found");
                    console.log("");
                     resolve('completed');
                }
            }
            else if (opsys == "linux") {
                // LinuxUserName = functions.getUserHome();
                var dir = './GitRepoList';
    
                var FileDir = dir + "/List.txt";
                if (fs.existsSync(FileDir)) {
                    FileText = fs.readFileSync(dir + "/List.txt");
                    var fileSet = functions.uniq(FileText.toString().split(/\r?\n/).clean(''));

                    console.log(chalk.blue("Repository's local directory") + "                                        |" + chalk.cyan(" Remote hosting website") + "                                     |"+chalk.green(" Project name")                        );

                    for (var i=0;i<fileSet.length;i++){
                        console.log(chalk.blue(fileSet[i].replace(/,/g,'\n').split(' ')[0].padEnd(70, ' ')) 
                        + chalk.cyan(fileSet[i].replace(/,/g,'\n').split(' ')[1].replace('https://','').replace('https:////','').replace('https////','').replace('https///','').split('/')[0].padEnd(60, ' ')) 
                        + chalk.green(fileSet[i].replace(/,/g,'\n').split(' ')[1].replace('https://','').replace('https:////','').replace('https////','').replace('https///','').split('/')[fileSet[i].replace(/,/g,'\n').split(' ').length]));
                    }
    
                    console.log("");
                     resolve('completed');

                }
                else {
                    console.log("no git branches found");
                     resolve('completed');
                }
            }
        }
        if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('localRepos',iniFile).localeCompare('No') === 0){
             resolve('completed');
        }
    });
}
function gitStatusTypeSimpleFilesTrackingCallingFunction(hashedDir){
    return new Promise((resolve, reject) => {
        // console.log(chalk.blue(functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('gitStatus',iniFile)));
        if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('gitStatus',iniFile).localeCompare('Yes') === 0){
            if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('FileTracking',iniFile).localeCompare('Yes')  === 0){
                var dir="";
                if (hashedDir.localeCompare('') === 0) {
                    dir = '';
                }
                else {
                    dir = 'cd ' + hashedDir + ' &&';
                }
                child = exec( dir + ' git status',
                { maxBuffer: 1024 * 1024 },
                function (error, stdout, stderr) {
                    // console.log(chalk.blue('\n'));
                    if (stdout.length == 0){
                        // console.log("no git branches found");
                        resolve('completed');
                    }
                    else{
                        console.log(chalk.black.bgMagenta(' FileTracking:       - git status simple file tracking type -                                                       \n'));
                        try {
                            
                            stdout = stdout.replace('Changes not staged for commit:','');
                            stdout = stdout.replace('(use "git add/rm <file>..." to update what will be committed)','');
                            stdout = stdout.replace('(use "git restore <file>..." to discard changes in working directory)','');
                            stdout = stdout.replace('(use "git add <file>..." to update what will be committed)','');
                            stdout = stdout.replace('On branch master','');
                            stdout = stdout.replace("Your branch is up to date with 'origin/master'.",'');
                            stdout = stdout.replace('(use "git add <file>..." to include in what will be committed)','');
                            stdout = stdout.replace('no changes added to commit (use "git add" and/or "git commit -a")','');

                            // console.log(stdout);


                            const strCopy = stdout.split('\n\n'); 
                            var strCopy1 = strCopy[1].split('\n');
                            

                            var filtered = strCopy.filter(function (el) {
                                return el != null && el != "" && el != "\n";
                            });

                            var lenghtOfModifiedOrRemovedFiles = filtered.length;


                            if (lenghtOfModifiedOrRemovedFiles<50 && strCopy[2].length < 1000 && strCopy[3] !== undefined){
                                try {
                                    if(strCopy[3].split('\n')!=null){
                                        var strCopy2 = strCopy[3].split('\n');

                                        if(lenghtOfModifiedOrRemovedFiles > 0)
                                        {
                                            console.log(chalk.yellow('Number of changed or modified files: ' + lenghtOfModifiedOrRemovedFiles));
                                        }
                                        if(lenghtOfModifiedOrRemovedFiles == 0)
                                        {
                                            console.log(chalk.green('Number of changed or modified files: ' + lenghtOfModifiedOrRemovedFiles));
                                        }

                                        for (var i=0;i<=strCopy.length;i++){

                                            var stringArray = strCopy;

                                            // console.log(strCopy.length);
                                            // try{
                                            //     console.log(strCopy[i]);
                                            // }
                                            // catch(e){
                                            //     console.log(e);
                                            // }

                                            //TODO
                                            // console.log(i);
                                            // console.log(strCopy[i]);

                                            if(strCopy[i] !== undefined){
                                                entry = strCopy[i].trim();
                                            }
                                            else{
                                                entry = strCopy[i-1].trim();
                                            }
                                            
                                            if(i === 0){
                                                console.log(chalk.rgb(0, 124, 255)("Modified files:"));
                                            }
                                            if(entry.includes('modified') && i === 0)
                                            {
                                                console.log(chalk.rgb(0, 124, 255)(entry.replace(/\t/g,'').replace(/modified:/g,'\nm: ')));
                                            }
                                            if(entry.includes('modified') && i > 0)
                                            {
                                                console.log(chalk.rgb(0, 124, 255)(entry.replace(/\t/g,'').replace(/modified:/g,'m: ')));
                                            }
                                        }

                                        for (var i=0;i<=strCopy.length;i++){

                                            if(strCopy[i] !== undefined){
                                                entry = strCopy[i].trim();
                                            }
                                            else{
                                                entry = strCopy[i-1].trim();
                                            }

                                            if(i === 0){
                                                console.log(chalk.rgb(123, 45, 67)("Deleted files:"));
                                            }
                                            if(entry.includes('deleted') && i === 0)
                                            {
                                                console.log(chalk.rgb(123, 45, 67)(entry.replace(/\t/g,'').replace(/deleted:/g,'\nd: ')));
                                            }
                                            if(entry.includes('deleted') && i > 0)
                                            {
                                                console.log(chalk.rgb(123, 45, 67)(entry.replace(/\t/g,'').replace(/deleted:/g,'d: ')));
                                            }
                                        }

                                        for (var i=0;i<=strCopy.length;i++){

                                            if(strCopy[i] !== undefined){
                                                entry = strCopy[i].trim();
                                            }
                                            else{
                                                entry = strCopy[i-1].trim();
                                            }

                                            if(!entry.includes('modified') && !entry.includes('deleted') && entry!== null && entry!=="" && entry!== " " && entry!== "  "  && i === 0)
                                            {
                                                console.log(chalk.rgb(123, 255, 67)(entry));
                                            }
                                            if(!entry.includes('modified') && !entry.includes('deleted') && entry!== null && entry!=="" && entry!== " " && entry!== "  "  && i > 0)
                                            {
                                                console.log(chalk.rgb(123, 255, 67)(entry));
                                            }
                                        }

                                        if(strCopy[2].toString().includes('Untracked files:')){

                                            var stringArray = strCopy[2].split('\n');

                                            for (var i=0;i<stringArray.length;i++){
                                                entry = stringArray[i].trim();

                                                if(i === 0){
                                                    console.log(chalk.gray("Untracked files:"));
                                                }

                                                if(entry!=="" && entry!== " " && entry!== "  " && entry!== null && !entry.includes("Untracked files:")){
                                                    console.log(chalk.gray("U: " + entry));
                                                }
                                            }
                                        }
                                        else if(strCopy2.includes('Untracked files:')){
                                            var stringArray = strCopy[2].split('\n');

                                            for (var i=0;i<stringArray.length;i++){
                                                entry = stringArray[i].trim();

                                                if(i === 0){
                                                    console.log(chalk.gray("Untracked files:"));
                                                }

                                                if(entry!=="" && entry!== " " && entry!== "  " && entry!== null && !entry.includes("Untracked files:")){
                                                    console.log(chalk.gray("U: " + entry));
                                                }
                                            }
                                        }

                                        if (stderr !== "") {
                                            console.log(chalk.yellow('stderr: ' + stderr));
                                        }
                                        if (error !== null) {
                                            console.log(chalk.red('exec error: ' + error));
                                        }

                                        console.log(chalk.blue(''));
                                        resolve('completed');

                                    }
                                }
                                catch (e) {
                                        console.log("x2 " + e.toString());
                                        // console.log(e);

                                        if(e.toString().includes('TypeError')){
                                            console.log(e);
                                        }

                                        if (strCopy1.includes("nothing to commit, working tree clean")){
                                            figlet('Everything is up to date !!!', function(err, data) {
                            
                                                    
                                                if(stdout.includes("nothing to commit, working tree clean")){
                                                    if (err) {
                                                        console.log('Something went wrong...');
                                                        console.dir(err);
                                    
                                                        return;
                                                    }
                                                    console.log(data)
                                                }
                            
                                                    console.log("");
                                                    resolve('completed');
            
                                            });
                                        }
                                        else if(strCopy1 != null){
                                            strCopy1 = strCopy1.filter(function(str) {
                                                return /\S/.test(str);
                                            });
                                
                                            if(strCopy1.length>topNumberOfLinesToDisplay*3 && truncation.localeCompare('Yes') === 0){

                                                for (var i=0;i<topNumberOfLinesToDisplay*2;i++){
                                                    entry = strCopy1[i].trim();
                                                    if(entry.includes('modified'))
                                                    {
                                                        console.log(chalk.rgb(0, 124, 255)(entry.replace(/modified:/g,'m: ')));
                                                    }
                                                }
                                                for (var i=0;i<topNumberOfLinesToDisplay*2;i++){
                                                    entry = strCopy1[i].trim();
                                                    if(entry.includes('deleted'))
                                                    {
                                                        console.log(chalk.rgb(123, 45, 67)(entry.replace(/deleted:/g,'d: ')));
                                                    }
                                                }
                                                for (var i=0;i<topNumberOfLinesToDisplay*2;i++){
                                                    entry = strCopy1[i].trim();
                                                    if(!entry.includes('modified') && !entry.includes('deleted') && entry!== null && entry!=="" && entry!== " " && entry!== "  ")
                                                    {
                                                        console.log(chalk.rgb(123, 255, 67)(entry));
                                                    }
                                                }

                                                console.log("");
                                                console.log("Too many changes to display: " + strCopy1.length + " The number is bigger than " + 2*topNumberOfLinesToDisplay + " . Instead of displaying all changes normally, changes are going to be shown in a truncated way. ");
                                            
                                            }
                                            else{

                                                strCopy1.forEach(function(entry) {

                                                    entry = entry.trim();
                                                    if(entry.includes('modified'))
                                                    {
                                                        console.log(chalk.rgb(0, 124, 255)(entry.replace(/modified:/g,'m: ')));
                                                    }

                                                });
                                                strCopy1.forEach(function(entry) {

                                                    entry = entry.trim();
                                                    if(entry.includes('deleted'))
                                                    {
                                                        console.log(chalk.rgb(123, 45, 67)(entry.replace(/deleted:/g,'d: ')));
                                                    }
                                                    
                                                });
                                                strCopy1.forEach(function(entry) {

                                                    entry = entry.trim();
                                                    if(!entry.includes('modified') && !entry.includes('deleted') && entry!== null && entry!=="" && entry!== " " && entry!== "  ")
                                                    {
                                                        console.log(chalk.rgb(123, 255, 67)(entry));
                                                    }
                                                });

                                            }

                                            strCopy2.forEach(function(entry) {

                                                entry = entry.trim();
                                                if(entry!=="" && entry!== " " && entry!== "  " && entry!== null){
                                                    console.log(chalk.gray("U: " + entry));
                                                }
                                            });
                                            
                                            if (stderr !== "") {
                                            console.log(chalk.yellow('stderr: ' + stderr));

                                            }
                                            if (error !== null) {
                                                console.log(chalk.red('exec error: ' + error));

                                            }

                                        }
                                        
                                    }
                                }
                        else{
                                console.log("There are too many changes files to display.");
                                resolve('completed');
                                console.log("");
                        }

                        }
                        catch (e) {
                            // console.log("x1 " + e.toString());
                            if (!e.toString().includes("TypeError:")) {
                                if(!stdout.includes("nothing to commit, working tree clean")){
                                    console.log(e);
                        
                                        figlet('Everything is up to date !!!', function(err, data) {
                        
                                                
                                            if(stdout.includes("nothing to commit, working tree clean")){
                                                if (err) {
                                                    console.log('Something went wrong...');
                                                    console.dir(err);
                                
                                                    return;
                                                }
                                                console.log(data)
                                            }
                        
                                            // if (BooleanTrackingIfMainHasBeenCalled === false){
                                                // In order to have statusbar in the beginning you have to have the main function called here.
                                                
                                                console.log(chalk.blue(''));
                                                resolve('completed');
                                        });
                                        
                                    }
                                    else{
                                        // if (BooleanTrackingIfMainHasBeenCalled === false){
                                            // In order to have statusbar in the beginning you have to have the main function called here.
                                            
                                            console.log(chalk.blue(''));
                                            resolve('completed');
                                    }
                            }
                            else{
                                console.log(" ______                    _   _     _               _                     _              _       _         _ _ _ ");
                                console.log("|  ____|                  | | | |   (_)             (_)                   | |            | |     | |       | | | |");
                                console.log("| |____   _____ _ __ _   _| |_| |__  _ _ __   __ _   _ ___   _   _ _ __   | |_ ___     __| | __ _| |_ ___  | | | |");
                                console.log("|  __\\ \\ / / _ \\ '__| | | | __| '_ \\| | '_ \\ / _` | | / __| | | | | '_ \\  | __/ _ \\   / _` |/ _` | __/ _ \\ | | | |");
                                console.log("| |___\\ V /  __/ |  | |_| | |_| | | | | | | | (_| | | \\__ \\ | |_| | |_) | | || (_) | | (_| | (_| | ||  __/ |_|_|_|");
                                console.log("|______\\_/ \\___|_|   \\__, |\\__|_| |_|_|_| |_|\\__, | |_|___/  \\__,_| .__/   \\__\\___/   \\__,_|\\__,_|\\__\\___| (_|_|_)");
                                console.log("                      __/ |                   __/ |               | |                                             ");
                                console.log("                     |___/                   |___/                |_|                                             ");
                                
                                console.log("");
                                resolve('completed');

                                // ______                    _   _     _               _                     _              _       _         _ _ _ 
                                // |  ____|                  | | | |   (_)             (_)                   | |            | |     | |       | | | |
                                // | |____   _____ _ __ _   _| |_| |__  _ _ __   __ _   _ ___   _   _ _ __   | |_ ___     __| | __ _| |_ ___  | | | |
                                // |  __\ \ / / _ \ '__| | | | __| '_ \| | '_ \ / _` | | / __| | | | | '_ \  | __/ _ \   / _` |/ _` | __/ _ \ | | | |
                                // | |___\ V /  __/ |  | |_| | |_| | | | | | | | (_| | | \__ \ | |_| | |_) | | || (_) | | (_| | (_| | ||  __/ |_|_|_|
                                // |______\_/ \___|_|   \__, |\__|_| |_|_|_| |_|\__, | |_|___/  \__,_| .__/   \__\___/   \__,_|\__,_|\__\___| (_|_|_)
                                //                       __/ |                   __/ |               | |                                             
                                //                      |___/                   |___/                |_|                                             
                                
                                // figlet('Everything is up to date !!!', function(err, data) {
                                //     if(stdout.includes("nothing to commit, working tree clean")){
                                //         if (err) {
                                //             console.log('Something went wrong...');
                                //             console.dir(err);
                                //             resolve('completed');
                                //             return;
                                //         }
                                //         console.log(data)
                                //     }
                                //     console.log("");
                                //     resolve('completed');
                                // });   
                            }
                        }
                    }
                });
            }
            else{
                resolve('completed');
            }
        }
        if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('gitStatus',iniFile).localeCompare('No') === 0){
            resolve('completed');
        }
    });
}
function gitStatusTypeOnelineGraphDecorateColorShortstatCallingFunction(hashedDir){
    return new Promise((resolve, reject) => {
        if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('gitStatus',iniFile).localeCompare('Yes') === 0){
            if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('GitGraph',iniFile).localeCompare('Yes') === 0){
                var dir="";
                if (hashedDir.localeCompare('') === 0) {
                    dir = '';
                }
                else {
                    dir = 'cd ' + hashedDir + ' &&';
                }
                child = exec( dir + ' git log --oneline --all --graph --color --shortstat --name-only -5'
                ,
                { maxBuffer: 1024 * 1024 },
                function (error, stdout, stderr) {
                    if (stdout.length == 0){
                        // console.log("no git branches found");
                        resolve('completed');
                    }
                    else{
                        console.log("");
                        console.log(chalk.black.bgMagenta(' GitGraph:           - git log, one line, graph, decorate, color, shortstat, only 5 -                               \n'));

                        // console.log(functions.getSizeOfStringLines(stdout));
                        // console.log(topNumberOfLinesToDisplay);

                        if(functions.getSizeOfStringLines(stdout) > topNumberOfLinesToDisplay  && truncation.localeCompare('Yes') === 0){
                            console.log(functions.limitStringLinesCount(stdout, topNumberOfLinesToDisplay));
                            console.log("There are " + functions.getSizeOfStringLines(stdout) + " lines of changes and so many cannot be displayed correctly. Because of that only the top " + topNumberOfLinesToDisplay + " are lines of output are displayed.");
                            console.log("");
                        }
                        else{
                            console.log(stdout);
                        }
                    }
                    resolve('completed');
                });
            }
            else{
                resolve('completed');
            }
        }
        if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('gitStatus',iniFile).localeCompare('No') === 0){
            resolve('completed');
        }
    });
}

function gitLogAbbreviatedCommitsInAshortstatOneLineGraph(){
    return new Promise((resolve) => {
        if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('gitStatus',iniFile).localeCompare('Yes') === 0){
            if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('AbbreviatedCommits',iniFile).localeCompare('Yes') === 0){
                var dir="";
                if (hashedDir.localeCompare('') === 0) {
                    dir = '';
                }
                else {
                    dir = 'cd ' + hashedDir + ' &&';
                }
                child = exec( dir + ' git log --oneline --abbrev-commit --all --graph --decorate --shortstat --max-count=5',
                { maxBuffer: 1024 * 1024 },
                function (error, stdout, stderr) {
                    if (stdout.length == 0){
                        // console.log("no git branches found");
                         resolve('completed');
                    }
                    else{
                        console.log(chalk.black.bgMagenta(' AbbreviatedCommits: - git log, abbreviated commits in a shortstat one line graph -                                 \n'));

                        for ( var i=0; i<stdout.split("\n").length-1;i++){
                            if(stdout.split("\n")[i].includes("*")){
                                
                                if(i%2 === 0 && i !== 0) {
                                    var stringToPrintDivisibleBy2 = "";
                                    stringToPrintDivisibleBy2 = stdout.split("\n")[i].substring(0,10) + chalk.magenta(stdout.split("\n")[i].substring(10,stdout.split("\n")[i].length));
                                    stringToPrintDivisibleBy2 = chalk.yellow(stringToPrintDivisibleBy2.substring(0,1)) + stringToPrintDivisibleBy2.substring(1,stringToPrintDivisibleBy2.length);
                                    stringToPrintDivisibleBy2 = stringToPrintDivisibleBy2.substring(0,6) + chalk.cyan(stringToPrintDivisibleBy2.substring(6,20)) + stringToPrintDivisibleBy2.substring(20,stringToPrintDivisibleBy2.length);

                                    console.log(stringToPrintDivisibleBy2);
                                }
                                if(i === 0){

                                    var stringToPrintNotDivisibleBy2 = "";

                                    if(stdout.split("\n")[i].toString().match(/[*] \w{7} /g,"") === null){
                                        stringToPrintNotDivisibleBy2 = stdout.split("\n")[i].toString().match(/[*][ ].{8}[ ][(]([^)]+)[)]/g,"").toString();
                                        stringToPrintNotDivisibleBy3 = chalk.magenta(stdout.split("\n")[i].toString().replace(/[*][ ].{8}[ ][(]([^)]+)[)]/g,""));

                                        stringToPrintNotDivisibleBy2 = chalk.yellow(stringToPrintNotDivisibleBy2.substring(0,1)) + stringToPrintNotDivisibleBy2.substring(1,stringToPrintNotDivisibleBy2.length);
                                        stringToPrintNotDivisibleBy2 = stringToPrintNotDivisibleBy2.substring(0,6) + chalk.cyan(stringToPrintNotDivisibleBy2.substring(6,20)) 
                                        + gradient.rainbow(stringToPrintNotDivisibleBy2.substring(20,stringToPrintNotDivisibleBy2.length));

                                        stringToPrintNotDivisibleBy2 = stringToPrintNotDivisibleBy2 + stringToPrintNotDivisibleBy3;

                                    }
                                    else{
                                        stringToPrintNotDivisibleBy2 = stdout.split("\n")[i].toString().match(/[*] \w{7} /g,"")[0] 
                                        + gradient.rainbow(stdout.split("\n")[i].toString().match(/\(([^)]+)\)/g,"")[0]) 
                                        + chalk.magenta(stdout.split("\n")[i].toString().replace(/[*] \w{7} \(([^)]+)\)/g,""));

                                        stringToPrintNotDivisibleBy2 = chalk.yellow(stringToPrintNotDivisibleBy2.substring(0,1)) + stringToPrintNotDivisibleBy2.substring(1,stringToPrintNotDivisibleBy2.length);
                                        stringToPrintNotDivisibleBy2 = stringToPrintNotDivisibleBy2.substring(0,6) + chalk.cyan(stringToPrintNotDivisibleBy2.substring(6,20)) 
                                        + stringToPrintNotDivisibleBy2.substring(20,stringToPrintNotDivisibleBy2.length);
                                    }

                                    console.log(stringToPrintNotDivisibleBy2);
                                }
                                
                            }
                            else{
                                var stringToPrint = stdout.split("\n")[i];
                                stringToPrint = chalk.red(stringToPrint.substring(0,1)) + stringToPrint.substring(1,stringToPrint.length);
                                stringToPrint0 = stringToPrint.split(",")[0];
                                if (stringToPrint0 === undefined){
                                    stringToPrint0 = "";
                                }
                                stringToPrint1 = stringToPrint.split(",")[1];
                                if (stringToPrint1 === undefined){
                                    stringToPrint1 = "";
                                }
                                stringToPrint2 = stringToPrint.split(",")[2];
                                if (stringToPrint2 === undefined){
                                    stringToPrint2 = "";
                                }
                                stringToPrint = chalk.gray(stringToPrint0) + "  " + chalk.green(stringToPrint1) + "  " + chalk.red(stringToPrint2);
                                console.log(stringToPrint);
                            }
                            
                        }

                        console.log("");
                    }
                     resolve('completed');
                    
                });
            }
            else{
                 resolve('completed');
            }
        }
        if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('gitStatus',iniFile).localeCompare('No') === 0){
             resolve('completed');
        }
    });
}

function ShowCommitsPerDayInANiceFormattedWay(){
    return new Promise((resolve) => {
        if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('gitStatus',iniFile).localeCompare('Yes') === 0){
            if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('CommitsPerDay',iniFile).localeCompare('Yes') === 0){
                var dir="";
                if (hashedDir.localeCompare('') === 0) {
                    dir = '';
                }
                else {
                    dir = 'cd ' + hashedDir + ' &&';
                }
                child = exec( dir + ' git log --date=short --pretty=format:%ad | sort -r | uniq -c',
                { maxBuffer: 1024 * 1024 },
                function (error, stdout, stderr) {
                    if (stdout.length == 0){
                        // console.log("no git branches found");
                         resolve('completed');
                    }
                    else{
                        console.log(chalk.black.bgMagenta(' CommitsPerDay:      - Show commits per day in a nice formatted way -                                               \n'));

                        var dateOneWeekAgo = new Date();
                        dateOneWeekAgo.setDate(dateOneWeekAgo.getDate()-7);

                        var date30DaysAgo = new Date();
                        date30DaysAgo.setDate(date30DaysAgo.getDate()-30);

                        var notPrintedWeek = true;
                        var notPrintedMonth = true;
                        var notYetPrintedTheMessage = true;
                        var noInputHasBeenPrintedToConsoleYet = true;

                        var SummarisedWeeklyCommits = 0;
                        var SummarisedMonthlyCommits = 0;

                        var ArrayWithAllPossibleColours = ['red', 'green', 'yellow', 'blue', 'magenta', 'cyan','white','gray'
                            ,'redBright','greenBright','yellowBright','blueBright','magentaBright','cyanBright','whiteBright'];

                        var StringThatContainsTheBarsForEachWeek = "";
                        var StringThatContainsTheBarsForEachMonth = "";

                        for ( var i=0; i<stdout.split("\n").length-1;i++){

                            // console.log(stdout.split("\n")[i].trim().split(" ").length);

                            if(stdout.split("\n")[i].trim().split(" ").length === 2){

                                var dateFromArrayFromPipedGitSortedCommand = new Date(stdout.split("\n")[i].trim().split(" ")[1]);
                                
                                // if(dateFromArrayFromPipedGitSortedCommand>date30DaysAgo){
                                    if(dateFromArrayFromPipedGitSortedCommand>dateOneWeekAgo){

                                        // console.log("SummarisedWeeklyCommits1");

                                        if(stdout.split("\n")[i].trim().split(" ")[0].length === 1){
                                            SummarisedWeeklyCommits = SummarisedWeeklyCommits + Number(stdout.split("\n")[i].trim().split(" ")[0]);
                                            // console.log(SummarisedWeeklyCommits);
                                        }
                                        else{
                                            SummarisedWeeklyCommits = SummarisedWeeklyCommits + Number(stdout.split("\n")[i].trim().split(" ")[0]);
                                            // console.log(SummarisedWeeklyCommits);
                                        }
                                        

                                    }
                                    else{

                                        if(stdout.split("\n")[i].trim().split(" ")[0].length === 1){
                                            SummarisedMonthlyCommits = SummarisedMonthlyCommits + Number(stdout.split("\n")[i].trim().split(" ")[0]);
                                            // console.log(SummarisedMonthlyCommits);
                                        }
                                        else{
                                            SummarisedMonthlyCommits = SummarisedMonthlyCommits + Number(stdout.split("\n")[i].trim().split(" ")[0]);
                                            // console.log(SummarisedMonthlyCommits);
                                        }

                                    }

                                // }
                                // else{
                                //     if(dateFromArrayFromPipedGitSortedCommand>dateOneWeekAgo){

                                //         // console.log("SummarisedWeeklyCommits1");

                                //         if(stdout.split("\n")[i].trim().split(" ")[0].length === 1){
                                //             SummarisedWeeklyCommits = SummarisedWeeklyCommits + Number(stdout.split("\n")[i].trim().split(" ")[0]);
                                //             // console.log(SummarisedWeeklyCommits);
                                //         }
                                //         else{
                                //             SummarisedWeeklyCommits = SummarisedWeeklyCommits + Number(stdout.split("\n")[i].trim().split(" ")[0]);
                                //             // console.log(SummarisedWeeklyCommits);
                                //         }
                                        

                                //     }
                                //     else{

                                //         if(stdout.split("\n")[i].trim().split(" ")[0].length === 1){
                                //             SummarisedMonthlyCommits = SummarisedMonthlyCommits + Number(stdout.split("\n")[i].trim().split(" ")[0]);
                                //             // console.log(SummarisedMonthlyCommits);
                                //         }
                                //         else{
                                //             SummarisedMonthlyCommits = SummarisedMonthlyCommits + Number(stdout.split("\n")[i].trim().split(" ")[0]);
                                //             // console.log(SummarisedMonthlyCommits);
                                //         }

                                //     }
                                // }

                            }
                            else{
                                // console.log("SummarisedWeeklyCommits3");
                                console.log(stdout.split("\n")[i].trim().split(" ")[0]);
                            }
                            
                        }
                        
                        for ( var i=0; i<stdout.split("\n").length-1;i++){
                            
                            if(stdout.split("\n")[i].trim().split(" ").length === 2){

                                // console.log(stdout.split("\n")[i].trim().split(" "));


                                var dateFromArrayFromPipedGitSortedCommand = new Date(stdout.split("\n")[i].trim().split(" ")[1]);

                                const CFonts = require('cfonts');

                                var prettyFont;

                                if(dateFromArrayFromPipedGitSortedCommand>date30DaysAgo){
                                    if(dateFromArrayFromPipedGitSortedCommand>dateOneWeekAgo){

                                        if(notPrintedWeek){
                                            console.log(chalk.blue("Activity in the past week: " + SummarisedWeeklyCommits + " commits"));
                                            notPrintedWeek = false;
                                        }

                                        var totalNumberOfcommitsPerLineWeek = stdout.split("\n")[i].trim().split(" ")[0];
                                        var totalNumberOfcommitsPerLineDividedWithRemainerWeek = totalNumberOfcommitsPerLineWeek / 10;

                                        var mathCeilWeek =  Math.ceil(totalNumberOfcommitsPerLineDividedWithRemainerWeek);
                                        var mathFloorWeek = Math.floor(totalNumberOfcommitsPerLineDividedWithRemainerWeek);

                                        var mathCeilForCalculationWeek=  0;

                                        StringThatContainsTheBarsForEachWeek="";

                                        if (mathFloorWeek === 0){
                                            prettyFont = CFonts.render("I".repeat(totalNumberOfcommitsPerLineWeek), {
                                            font: 'tiny',              // define the font face
                                            align: 'left',             // define text alignment
                                            colors: ['cyan'],          // define all colors
                                            background: 'transparent',  // define the background color, you can also use `backgroundColor` here as key
                                            letterSpacing: 0,           // define letter spacing
                                            lineHeight: 1,              // define the line height
                                            space: false,               // define if the output text should have empty lines on top and on the bottom
                                            maxLength: '0',             // define how many character can be on one line
                                            gradient: false,            // define your two gradient colors
                                            independentGradient: false, // define if you want to recalculate the gradient for each new line
                                            transitionGradient: false,  // define if this is a transition between colors directly
                                            env: 'node'                 // define the environment CFonts is being executed in
                                            });

                                            prettyFont.string  // the ansi string for sexy console font
                                            prettyFont.array   // returns the array for the output
                                            prettyFont.lines   // returns the lines used
                                            prettyFont.options // returns the options used
                
                                            StringThatContainsTheBarsForEachWeek = functions.splitString(prettyFont.string.replace(' ','').replace('\n','').trim());

                                            var dateObjectToGetDay = new Date(stdout.split("\n")[i].trim().split(" ")[1].replace(/-/g,"/"));
                                            
                                            try{
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 6){
                                                    if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "    " + chalk.red(totalNumberOfcommitsPerLineWeek) + "  " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "    " + chalk.red(totalNumberOfcommitsPerLineWeek) + " " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                }
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 7){
                                                    if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "   " + chalk.red(totalNumberOfcommitsPerLineWeek) + "  " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false; 
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "   " + chalk.red(totalNumberOfcommitsPerLineWeek) + " " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false; 
                                                    }
                                                }
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 8){
                                                    if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "  " + chalk.red(totalNumberOfcommitsPerLineWeek) + "  " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "  " + chalk.red(totalNumberOfcommitsPerLineWeek) + " " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                }
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 9){
                                                    if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + " " + chalk.red(totalNumberOfcommitsPerLineWeek) + "  " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false; 
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + " " + chalk.red(totalNumberOfcommitsPerLineWeek) + " " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                }
                                            }
                                            catch(e){
                                                // console.log("x1 " + e.toString());
                                                if(e.toString().includes("TypeError:")){
                                                    
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 6){
                                                        if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "    " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "    " + chalk.red(10) + " " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 7){
                                                        if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "   " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "   " + chalk.red(10) + " " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 8){
                                                        if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "  " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "  " + chalk.red(10) + " " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 9){
                                                        if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + " " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + " " + chalk.red(10) + " " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if (mathFloorWeek != 0){
                                            for(var ii=0;ii<mathCeilWeek;ii++){
                                                if(mathFloorWeek != mathCeilForCalculationWeek){

                                                    if(ii<15){
                                                        if (mathFloor != 0){
                                                            prettyFont = CFonts.render("I".repeat(10), {
                                                            font: 'tiny',              // define the font face
                                                            align: 'left',             // define text alignment
                                                            colors: [ArrayWithAllPossibleColours[ii]],          // define all colors
                                                            background: 'transparent',  // define the background color, you can also use `backgroundColor` here as key
                                                            letterSpacing: 0,           // define letter spacing
                                                            lineHeight: 1,              // define the line height
                                                            space: false,               // define if the output text should have empty lines on top and on the bottom
                                                            maxLength: '0',             // define how many character can be on one line
                                                            gradient: false,            // define your two gradient colors
                                                            independentGradient: false, // define if you want to recalculate the gradient for each new line
                                                            transitionGradient: false,  // define if this is a transition between colors directly
                                                            env: 'node'                 // define the environment CFonts is being executed in
                                                            });
                    
                                                            prettyFont.string  // the ansi string for sexy console font
                                                            prettyFont.array   // returns the array for the output
                                                            prettyFont.lines   // returns the lines used
                                                            prettyFont.options // returns the options used
                                
                                                            StringThatContainsTheBarsForEachWeek = StringThatContainsTheBarsForEachWeek + functions.splitString(prettyFont.string.replace(' ','').replace('\n','').trim());
                                                            // console.log(StringThatContainsTheBarsForEachMonth);
                                                        }
                                                    }
                                                    else{
                                                        prettyFont = CFonts.render("I".repeat(10), {
                                                            font: 'tiny',              // define the font face
                                                            align: 'left',             // define text alignment
                                                            colors: ['blue'],          // define all colors
                                                            background: 'transparent',  // define the background color, you can also use `backgroundColor` here as key
                                                            letterSpacing: 0,           // define letter spacing
                                                            lineHeight: 1,              // define the line height
                                                            space: false,               // define if the output text should have empty lines on top and on the bottom
                                                            maxLength: '0',             // define how many character can be on one line
                                                            gradient: false,            // define your two gradient colors
                                                            independentGradient: false, // define if you want to recalculate the gradient for each new line
                                                            transitionGradient: false,  // define if this is a transition between colors directly
                                                            env: 'node'                 // define the environment CFonts is being executed in
                                                            });
                    
                                                            prettyFont.string  // the ansi string for sexy console font
                                                            prettyFont.array   // returns the array for the output
                                                            prettyFont.lines   // returns the lines used
                                                            prettyFont.options // returns the options used
                                
                                                            StringThatContainsTheBarsForEachWeek = StringThatContainsTheBarsForEachWeek + functions.splitString(prettyFont.string.replace(' ','').replace('\n','').trim());  
                                                            // console.log(StringThatContainsTheBarsForEachMonth);
                                                    }

                                                    // console.log(mathCeilForCalculation);
                                                    mathCeilForCalculationWeek = mathCeilForCalculationWeek + 1;
                                                }
                                                else{
                                                    var remainder = totalNumberOfcommitsPerLineWeek % 10;

                                                    prettyFont = CFonts.render("I".repeat(remainder), {
                                                        font: 'tiny',              // define the font face
                                                        align: 'left',             // define text alignment
                                                        colors: ['cyan'],          // define all colors
                                                        background: 'transparent',  // define the background color, you can also use `backgroundColor` here as key
                                                        letterSpacing: 0,           // define letter spacing
                                                        lineHeight: 1,              // define the line height
                                                        space: false,               // define if the output text should have empty lines on top and on the bottom
                                                        maxLength: '0',             // define how many character can be on one line
                                                        gradient: false,            // define your two gradient colors
                                                        independentGradient: false, // define if you want to recalculate the gradient for each new line
                                                        transitionGradient: false,  // define if this is a transition between colors directly
                                                        env: 'node'                 // define the environment CFonts is being executed in
                                                        });
                
                                                        prettyFont.string  // the ansi string for sexy console font
                                                        prettyFont.array   // returns the array for the output
                                                        prettyFont.lines   // returns the lines used
                                                        prettyFont.options // returns the options used
                            
                                                        StringThatContainsTheBarsForEachWeek = StringThatContainsTheBarsForEachWeek + functions.splitString(prettyFont.string.replace(' ','').replace('\n','').trim());  
                                                        // console.log(StringThatContainsTheBarsForEachMonth);
                                                }
                                            }
                                            
                                            var dateObjectToGetDay = new Date(stdout.split("\n")[i].trim().split(" ")[1].replace(/-/g,"/"));
                                            
                                            try{
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 6){
                                                    if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "    " + chalk.red(totalNumberOfcommitsPerLineWeek) + "  " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "    " + chalk.red(totalNumberOfcommitsPerLineWeek) + " " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                }
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 7){
                                                    if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "   " + chalk.red(totalNumberOfcommitsPerLineWeek) + "  " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "   " + chalk.red(totalNumberOfcommitsPerLineWeek) + " " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                }
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 8){
                                                    if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "  " + chalk.red(totalNumberOfcommitsPerLineWeek) + "  " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "  " + chalk.red(totalNumberOfcommitsPerLineWeek) + " " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                }
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 9){
                                                    if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + " " + chalk.red(totalNumberOfcommitsPerLineWeek) + "  " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + " " + chalk.red(totalNumberOfcommitsPerLineWeek) + " " + StringThatContainsTheBarsForEachWeek);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                }
                                            }
                                            catch(e){
                                                // console.log("x2 " + e.toString());
                                                if(e.toString().includes("TypeError:")){
                                                    
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 6){
                                                        if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "    " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "    " + chalk.red(10) + " " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 7){
                                                        if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "   " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "   " + chalk.red(10) + " " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 8){
                                                        if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "  " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "  " + chalk.red(10) + " " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 9){
                                                        if(totalNumberOfcommitsPerLineWeek.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + " " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + " " + chalk.red(10) + " " + StringThatContainsTheBarsForEachWeek);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                }
                                            }
                                        }       
                                    }
                                    else{

                                        if(notPrintedMonth){
                                            console.log("");
                                            console.log(chalk.blue("Activity in the past month (excluding last week): " + SummarisedMonthlyCommits+ " commits" + " | A total of " + (Number(SummarisedWeeklyCommits) + Number(SummarisedMonthlyCommits)) + " commits (for the whole month)"));
                                            notPrintedMonth = false;
                                        }

                                        var totalNumberOfcommitsPerLine = stdout.split("\n")[i].trim().split(" ")[0];
                                        var totalNumberOfcommitsPerLineDividedWithRemainer = totalNumberOfcommitsPerLine / 10;

                                        var mathCeil =  Math.ceil(totalNumberOfcommitsPerLineDividedWithRemainer);
                                        var mathFloor = Math.floor(totalNumberOfcommitsPerLineDividedWithRemainer);

                                        var mathCeilForCalculation =  0;

                                        StringThatContainsTheBarsForEachMonth="";

                                        if (mathFloor != 0){
                                            for(var ii=0;ii<mathCeil;ii++){
                                                if(mathFloor != mathCeilForCalculation){

                                                    if(ii<15){
                                                        if (mathFloor != 0){
                                                            prettyFont = CFonts.render("I".repeat(10), {
                                                            font: 'tiny',              // define the font face
                                                            align: 'left',             // define text alignment
                                                            colors: [ArrayWithAllPossibleColours[ii]],          // define all colors
                                                            background: 'transparent',  // define the background color, you can also use `backgroundColor` here as key
                                                            letterSpacing: 0,           // define letter spacing
                                                            lineHeight: 1,              // define the line height
                                                            space: false,               // define if the output text should have empty lines on top and on the bottom
                                                            maxLength: '0',             // define how many character can be on one line
                                                            gradient: false,            // define your two gradient colors
                                                            independentGradient: false, // define if you want to recalculate the gradient for each new line
                                                            transitionGradient: false,  // define if this is a transition between colors directly
                                                            env: 'node'                 // define the environment CFonts is being executed in
                                                            });
                    
                                                            prettyFont.string  // the ansi string for sexy console font
                                                            prettyFont.array   // returns the array for the output
                                                            prettyFont.lines   // returns the lines used
                                                            prettyFont.options // returns the options used
                                
                                                            StringThatContainsTheBarsForEachMonth = StringThatContainsTheBarsForEachMonth + functions.splitString(prettyFont.string.replace(' ','').replace('\n','').trim());
                                                            // console.log(StringThatContainsTheBarsForEachMonth);
                                                        }
                                                    }
                                                    else{
                                                        prettyFont = CFonts.render("I".repeat(10), {
                                                            font: 'tiny',              // define the font face
                                                            align: 'left',             // define text alignment
                                                            colors: ['blue'],          // define all colors
                                                            background: 'transparent',  // define the background color, you can also use `backgroundColor` here as key
                                                            letterSpacing: 0,           // define letter spacing
                                                            lineHeight: 1,              // define the line height
                                                            space: false,               // define if the output text should have empty lines on top and on the bottom
                                                            maxLength: '0',             // define how many character can be on one line
                                                            gradient: false,            // define your two gradient colors
                                                            independentGradient: false, // define if you want to recalculate the gradient for each new line
                                                            transitionGradient: false,  // define if this is a transition between colors directly
                                                            env: 'node'                 // define the environment CFonts is being executed in
                                                            });
                    
                                                            prettyFont.string  // the ansi string for sexy console font
                                                            prettyFont.array   // returns the array for the output
                                                            prettyFont.lines   // returns the lines used
                                                            prettyFont.options // returns the options used
                                
                                                            StringThatContainsTheBarsForEachMonth = StringThatContainsTheBarsForEachMonth + functions.splitString(prettyFont.string.replace(' ','').replace('\n','').trim());  
                                                            // console.log(StringThatContainsTheBarsForEachMonth);
                                                    }

                                                    // console.log(mathCeilForCalculation);
                                                    mathCeilForCalculation = mathCeilForCalculation + 1;
                                                }
                                                else{
                                                    var remainder = totalNumberOfcommitsPerLine % 10;

                                                    prettyFont = CFonts.render("I".repeat(remainder), {
                                                        font: 'tiny',              // define the font face
                                                        align: 'left',             // define text alignment
                                                        colors: ['cyan'],          // define all colors
                                                        background: 'transparent',  // define the background color, you can also use `backgroundColor` here as key
                                                        letterSpacing: 0,           // define letter spacing
                                                        lineHeight: 1,              // define the line height
                                                        space: false,               // define if the output text should have empty lines on top and on the bottom
                                                        maxLength: '0',             // define how many character can be on one line
                                                        gradient: false,            // define your two gradient colors
                                                        independentGradient: false, // define if you want to recalculate the gradient for each new line
                                                        transitionGradient: false,  // define if this is a transition between colors directly
                                                        env: 'node'                 // define the environment CFonts is being executed in
                                                        });
                
                                                        prettyFont.string  // the ansi string for sexy console font
                                                        prettyFont.array   // returns the array for the output
                                                        prettyFont.lines   // returns the lines used
                                                        prettyFont.options // returns the options used
                            
                                                        StringThatContainsTheBarsForEachMonth = StringThatContainsTheBarsForEachMonth + functions.splitString(prettyFont.string.replace(' ','').replace('\n','').trim());  
                                                        // console.log(StringThatContainsTheBarsForEachMonth);
                                                }
                                            }

                                            var dateObjectToGetDay = new Date(stdout.split("\n")[i].trim().split(" ")[1].replace(/-/g,"/"));

                                            try{
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 6){
                                                    if(totalNumberOfcommitsPerLine.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "    " + chalk.red(totalNumberOfcommitsPerLine) + "  " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "    " + chalk.red(totalNumberOfcommitsPerLine) + " " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                }
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 7){
                                                    if(totalNumberOfcommitsPerLine.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "   " + chalk.red(totalNumberOfcommitsPerLine) + "  " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "   " + chalk.red(totalNumberOfcommitsPerLine) + " " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                }
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 8){
                                                    if(totalNumberOfcommitsPerLine.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "  " + chalk.red(totalNumberOfcommitsPerLine) + "  " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "  " + chalk.red(totalNumberOfcommitsPerLine) + " " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                }
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 9){
                                                    if(totalNumberOfcommitsPerLine.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + " " + chalk.red(totalNumberOfcommitsPerLine) + "  " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + " " + chalk.red(totalNumberOfcommitsPerLine) + " " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                }
                                            }
                                            catch(e){
                                                // console.log("x3 " + e.toString());
                                                if(e.toString().includes("TypeError:")){
                                                    
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 6){
                                                        if(totalNumberOfcommitsPerLine.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "    " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "    " + chalk.red(10) + " " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 7){
                                                        if(totalNumberOfcommitsPerLine.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "   " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "   " + chalk.red(10) + " " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 8){
                                                        if(totalNumberOfcommitsPerLine.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "  " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "  " + chalk.red(10) + " " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 9){
                                                        if(totalNumberOfcommitsPerLine.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + " " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + " " + chalk.red(10) + " " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                }
                                            }
                                        }       

                                        if (mathFloor === 0){
                                            prettyFont = CFonts.render("I".repeat(totalNumberOfcommitsPerLine), {
                                            font: 'tiny',              // define the font face
                                            align: 'left',             // define text alignment
                                            colors: ['cyan'],          // define all colors
                                            background: 'transparent',  // define the background color, you can also use `backgroundColor` here as key
                                            letterSpacing: 0,           // define letter spacing
                                            lineHeight: 1,              // define the line height
                                            space: false,               // define if the output text should have empty lines on top and on the bottom
                                            maxLength: '0',             // define how many character can be on one line
                                            gradient: false,            // define your two gradient colors
                                            independentGradient: false, // define if you want to recalculate the gradient for each new line
                                            transitionGradient: false,  // define if this is a transition between colors directly
                                            env: 'node'                 // define the environment CFonts is being executed in
                                            });

                                            prettyFont.string  // the ansi string for sexy console font
                                            prettyFont.array   // returns the array for the output
                                            prettyFont.lines   // returns the lines used
                                            prettyFont.options // returns the options used
                
                                            StringThatContainsTheBarsForEachMonth = functions.splitString(prettyFont.string.replace(' ','').replace('\n','').trim());


                                            var dateObjectToGetDay = new Date(stdout.split("\n")[i].trim().split(" ")[1].replace(/-/g,"/"));
                                            
                                            try{
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 6){
                                                    if(totalNumberOfcommitsPerLine.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "    " + chalk.red(totalNumberOfcommitsPerLine) + "  " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "    " + chalk.red(totalNumberOfcommitsPerLine) + " " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                }
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 7){
                                                    if(totalNumberOfcommitsPerLine.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "   " + chalk.red(totalNumberOfcommitsPerLine) + "  " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "   " + chalk.red(totalNumberOfcommitsPerLine) + " " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                }
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 8){
                                                    if(totalNumberOfcommitsPerLine.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "  " + chalk.red(totalNumberOfcommitsPerLine) + "  " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + "  " + chalk.red(totalNumberOfcommitsPerLine) + " " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                }
                                                if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 9){
                                                    if(totalNumberOfcommitsPerLine.length === 1){
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + " " + chalk.red(totalNumberOfcommitsPerLine) + "  " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                    else{
                                                        console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                        + " " + chalk.red(totalNumberOfcommitsPerLine) + " " + StringThatContainsTheBarsForEachMonth);
                                                        noInputHasBeenPrintedToConsoleYet = false;  
                                                    }
                                                }
                                            }
                                            catch(e){
                                                // console.log("x4 " + e.toString());
                                                if(e.toString().includes("TypeError:")){
                                                    
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 6){
                                                        if(totalNumberOfcommitsPerLine.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "    " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "    " + chalk.red(10) + " " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 7){
                                                        if(totalNumberOfcommitsPerLine.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "   " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "   " + chalk.red(10) + " " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 8){
                                                        if(totalNumberOfcommitsPerLine.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "  " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + "  " + chalk.red(10) + " " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                    if(dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }).length === 9){
                                                        if(totalNumberOfcommitsPerLine.length === 1){
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + " " + chalk.red(10) + "  " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                        else{
                                                            console.log(stdout.split("\n")[i].trim().split(" ")[1] + " " + dateObjectToGetDay.toLocaleDateString('en-US', { weekday: 'long' }) 
                                                            + " " + chalk.red(10) + " " + StringThatContainsTheBarsForEachMonth);
                                                            noInputHasBeenPrintedToConsoleYet = false;  
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else{
                                    // console.log("SummarisedWeeklyCommits55 " + stdout.split("\n")[i].trim().split(" "));
                                    if(notYetPrintedTheMessage === true){
                                        if(noInputHasBeenPrintedToConsoleYet === true){
                                            console.log("There are no commits made in the past 30 days.");
                                            notYetPrintedTheMessage = false;
                                        }
                                    }

                                }

                            }
                            else{
                                console.log(stdout.split("\n")[i].trim().split(" ")[0]);
                            }
                        }

                        console.log("");
                    }
                     resolve('completed');
                });
            }
            else{
                 resolve('completed');
            }
        }
        if (functionsForReadingAndWritingToIniConfigFiles.ReadFromFile('gitStatus',iniFile).localeCompare('No') === 0){
             resolve('completed');
        }
    });
}

var main = function main(){

    message: console.log(chalk.black.bgBlackBright (' Choose an option:                                                                                                      '));
   
    var choicesListArrayInitialized = [
        chalk.cyan('SYNC PUSH:      Make a local commit and push             (git add . && git add -A  && git commit -m" + arg1var + " && start cmd /k git push)'),
        chalk.cyan('SYNC PUSH:      Make a local commit                      (git add . && git add -A  && git commit -m" + arg1var ")'), 
        chalk.cyan('SYNC PUSH:      Push to remote                           (git push)'), 
        chalk.cyan('SYNC PUSH:      Force push to remote                     (git push -f origin master)'), 
        chalk.blueBright('SYNC PULL:      Pull origin master                       (git pull origin master)'), 
        chalk.blueBright('SYNC PULL:      Pull rebase                              (git pull --rebase)'), 
        chalk.blueBright('SYNC PULL:      Pull                                     (git pull)'), 
        chalk.blueBright('SYNC PULL:      Clone from url                           (git clone <*url*>)'), 
        chalk.blueBright('SYNC PULL:      Clone from list of repos                 (git clone <*list*>)'), 
        chalk.green('BRANCHING:      Make new branch                          (git checkout -b <*name*>)'),
        chalk.green('BRANCHING:      Show branches                            (git branch -a)'),
        chalk.green('BRANCHING:      Switch branches                          (git checkout <*name*>)'),
        chalk.green('BRANCHING:      Remove branch                            (git branch -D <*name*>)'),
        chalk.green('BRANCHING:      Remove remote branch                     (git push <listOfRemote/origin> --delete <*name*>)'),
        chalk.green('BRANCHING:      Merge current branch with another        (git merge <*name*>)'),
        chalk.redBright('REVERT:         Reverts the HEAD and the last commit     (git reset HEAD~1)'),
        chalk.redBright('REVERT:         Like above, but leaves files and index   (git reset --soft HEAD~1)'),
        chalk.redBright('REVERT:         Reverts and resets files to past state   (git reset --hard HEAD~1)'),
        chalk.yellow('INFO:           Visualise git                            (<*list of options*>)'), 
        chalk.yellow('INFO:           Get list of repos                        (gets <*list*>)'),
        chalk.yellow('INFO:           git reflog                               (git reflog)'),
        chalk.yellow('INFO:           git show                                 (git show)'),  
        chalk.yellow('INFO:           git show all remotes                     (git remote -v)'), 
        chalk.yellow('INFO:           git diff                                 (git diff)'), 
        chalk.yellow('INFO:           git status                               (git status)'), 
        chalk.greenBright('TAGGING:        List git tags                            (git tag)'),
        chalk.greenBright('TAGGING:        Show info about git tags                 (git show <*name*>)'),
        chalk.greenBright('TAGGING:        Create lightweight git tags              (git tag <*name*>)'),
        chalk.greenBright('TAGGING:        Create annotated git tags                (git tag -a <*name*> -m <*message*>)'),  
        chalk.greenBright('TAGGING:        Push git tags to remote                  (git push --tags)'),
        chalk.greenBright('TAGGING:        Delete tags                              (git tag -d <*name*>)'),  
        chalk.magentaBright('REPO:           Register local repo                      (writes repo name in $User\\Documents\\GitRepoList\\List.txt)'), 
        chalk.magentaBright('REPO:           Open local repo                          (reads repo name from $User\\Documents\\GitRepoList\\List.txt)'), 
        chalk.magentaBright('REPO:           Go to remote origin                      (git config --get remote.origin.url)'), 
        chalk.hex('#a434eb')('UTILITY:        git config core.autocrlf true            (git config core.autocrlf true)'), 
        chalk.blackBright('HELP:           Show infographic                         (start http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf )'), 
        chalk.blackBright('HELP:           Show ohshitgit.com                       (start https://ohshitgit.com/ )'),
        chalk.bold.red('API TOKENS:     List API TOKENS'),
        chalk.bold.red('API TOKENS:     Create API TOKENS'),
        chalk.bold.red('API TOKENS:     Delete API TOKENS'),
        gradient.rainbow('SETTINGS:       Modify startup data fields.'),
        gradient.rainbow('SETTINGS:       Modify status visualisation types.'),
        gradient.rainbow('SETTINGS:       Modify miscellaneous settings.'),
        gradient.rainbow('SETTINGS:       Modify credential-manager settings.')
    ];

    var choicesListArrayNotInitialized = [
        chalk.green('INIT:           initialize git                           (git init)'), 
        chalk.yellow('INFO:           Visualise git                            (<*list of options*>)'), 
        chalk.yellow('INFO:           Get list of repos                        (gets <*list*>)'),
        chalk.yellow('INFO:           git reflog                               (git reflog)'),
        chalk.yellow('INFO:           git show                                 (git show)'),  
        chalk.yellow('INFO:           git show all remotes                     (git remote -v)'), 
        chalk.yellow('INFO:           git diff                                 (git diff)'), 
        chalk.yellow('INFO:           git status                               (git status)'), 
        chalk.greenBright('TAGGING:        List git tags                            (git tag)'),
        chalk.greenBright('TAGGING:        Show info about git tags                 (git show <*name*>)'),
        chalk.greenBright('TAGGING:        Create lightweight git tags              (git tag <*name*>)'),
        chalk.greenBright('TAGGING:        Create annotated git tags                (git tag -a <*name*> -m <*message*>)'),  
        chalk.greenBright('TAGGING:        Push git tags to remote                  (git push --tags)'),
        chalk.greenBright('TAGGING:        Delete tags                              (git tag -d <*name*>)'),  
        chalk.magentaBright('REPO:           Register local repo                      (writes repo name in $User\\Documents\\GitRepoList\\List.txt)'), 
        chalk.magentaBright('REPO:           Open local repo                          (reads repo name from $User\\Documents\\GitRepoList\\List.txt)'), 
        chalk.magentaBright('REPO:           Go to remote origin                      (git config --get remote.origin.url)'), 
        chalk.hex('#a434eb')('UTILITY:        git config core.autocrlf true            (git config core.autocrlf true)'), 
        chalk.blackBright('HELP:           Show infographic                         (start http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf )'), 
        chalk.blackBright('HELP:           Show ohshitgit.com                       (start https://ohshitgit.com/ )'),
        chalk.bold.red('API TOKENS:     List API TOKENS'),
        chalk.bold.red('API TOKENS:     Create API TOKENS'),
        chalk.bold.red('API TOKENS:     Delete API TOKENS'),
        gradient.rainbow('SETTINGS:       Modify startup data fields.'),
        gradient.rainbow('SETTINGS:       Modify status visualisation types.'),
        gradient.rainbow('SETTINGS:       Modify miscellaneous settings.'),
        gradient.rainbow('SETTINGS:       Modify credential-manager settings.')
    ];

    if(directoryChosen.localeCompare('current') === 0 && currentInitializedStatus){
        var choicesListArray = choicesListArrayInitialized;
    }
    else if (directoryChosen.localeCompare('current') === 0 && !currentInitializedStatus){
        var choicesListArray = choicesListArrayNotInitialized;
    }
    if(directoryChosen.localeCompare('hashed') === 0 && hashedInitializedStatus){
        var choicesListArray = choicesListArrayInitialized;
    }
    else if (directoryChosen.localeCompare('hashed') === 0 && !hashedInitializedStatus){
        var choicesListArray = choicesListArrayNotInitialized;
    }

    inquirer
        .prompt([{
            type: 'list',
            name: 'option',
            pageSize: '44',
            choices: choicesListArray,
        },])
        .then(answers => {     
            if (answers.option == chalk.green('INIT:           initialize git                           (git init)')) {
                optionsList.Init(hashedDir);
            }
            if (answers.option == chalk.cyan('SYNC PUSH:      Make a local commit and push             (git add . && git add -A  && git commit -m" + arg1var + " && start cmd /k git push)')) {
                optionsList.CommitAndPush(hashedDir);
            }
            if (answers.option == chalk.cyan('SYNC PUSH:      Make a local commit                      (git add . && git add -A  && git commit -m" + arg1var ")')) {
                optionsList.Commit(hashedDir);
            }
            if (answers.option == chalk.cyan('SYNC PUSH:      Push to remote                           (git push)')) {
                optionsList.Sync_GitPush_ExternalCall(hashedDir);
            }
            if (answers.option == chalk.cyan('SYNC PUSH:      Force push to remote                     (git push -f origin master)')) {
                optionsList.ForcePush(hashedDir);
            }
            if (answers.option == chalk.blueBright('SYNC PULL:      Pull origin master                       (git pull origin master)')) {
                optionsList.GitPUllOriginMaster(hashedDir);
            }
            if (answers.option == chalk.blueBright('SYNC PULL:      Pull rebase                              (git pull --rebase)')) {
                optionsList.GitPUllRebase(hashedDir);
            }
            if (answers.option == chalk.blueBright('SYNC PULL:      Pull                                     (git pull)')) {
                optionsList.GitPUll(hashedDir);
            }
            if (answers.option ==  chalk.blueBright('SYNC PULL:      Clone from url                           (git clone <*url*>)')) {
                optionsList.GitCloneFromURl(hashedDir);
            }
            if (answers.option == chalk.blueBright('SYNC PULL:      Clone from list of repos                 (git clone <*list*>)')) {
                optionsList.GetListOfRepos(hashedDir, answers);
            }
            if (answers.option == chalk.green('BRANCHING:      Make new branch                          (git checkout -b <*name*>)')) {
                BRANCHINGmakenewbranches.MakeNewBranches(hashedDir);
            }
            if (answers.option == chalk.green('BRANCHING:      Show branches                            (git branch -a)')) {
                BRANCHINGShowbranches.ShowBranches(hashedDir);
            }
            if (answers.option == chalk.green('BRANCHING:      Switch branches                          (git checkout <*name*>)')) {
                BRANCHINGSwitchbranches.SwitchBranches(hashedDir);
            }
            if (answers.option == chalk.green('BRANCHING:      Remove branch                            (git branch -D <*name*>)')) {
                BRANCHINGRemovebranch.RemoveBranch(hashedDir);
            }
            if (answers.option == chalk.green('BRANCHING:      Remove remote branch                     (git push <listOfRemote/origin> --delete <*name*>)')) {
                BRANCHINGRemoveremotebranch.BRANCHINGRemoveremotebranch(hashedDir);
            }
            if (answers.option == chalk.green('BRANCHING:      Merge current branch with another        (git merge <*name*>)')) {
                BRANCHINGMergebranch.MergeBranches(hashedDir);
            }
            if (answers.option == chalk.redBright('REVERT:         Reverts the HEAD and the last commit     (git reset HEAD~1)')) {
                REVERTReturnthelastcommit.REVERTHeadAndLastCommit(hashedDir);
            }
            if (answers.option == chalk.redBright('REVERT:         Like above, but leaves files and index   (git reset --soft HEAD~1)')) {
                REVERTHeadAndLastCommitSoft.REVERTHeadAndLastCommitSoft(hashedDir);
            }
            if (answers.option == chalk.redBright('REVERT:         Reverts and resets files to past state   (git reset --hard HEAD~1)')) {
                REVERTHeadAndLastCommitHard.REVERTHeadAndLastCommitHard(hashedDir);
            }
            if (answers.option == chalk.yellow('INFO:           Visualise git                            (<*list of options*>)')) {
                INFOVisualisegit.VisualiseGit(hashedDir);
            }
            if (answers.option == chalk.yellow('INFO:           Get list of repos                        (gets <*list*>)')) {
                INFOGetlistofrepos.GetListOfRepos(hashedDir, answers);
            }
            if (answers.option == chalk.yellow('INFO:           git reflog                               (git reflog)')) {
                INFOGitReflog.GitReflog(hashedDir);
            }
            if (answers.option == chalk.yellow('INFO:           git show                                 (git show)')) {
                INFOGitshow.GitShow(hashedDir);
            }
            if (answers.option == chalk.yellow('INFO:           git show all remotes                     (git remote -v)')) {
                INFOGitShowAllRemotes.GitShowAllRemotes(hashedDir);
            }
            if (answers.option == chalk.yellow('INFO:           git diff                                 (git diff)')) {
                INFOGitdiff.GitDiff(hashedDir);
            }
            if (answers.option == chalk.yellow('INFO:           git status                               (git status)')) {
                INFOGitStatus.gitStatus(hashedDir);
            }
            if (answers.option == chalk.greenBright('TAGGING:        List git tags                            (git tag)')) {
                TaggingListAllTags.ListAllTags(hashedDir);
            }
            if (answers.option == chalk.greenBright('TAGGING:        Show info about git tags                 (git show <*name*>)')) {
                TaggingShow.TaggingShow(hashedDir);
            }
            if (answers.option == chalk.greenBright('TAGGING:        Create lightweight git tags              (git tag <*name*>)')) {
                TaggingCreateLightweightTags.TaggingCreateLightweightTags(hashedDir);
            }
            if (answers.option == chalk.greenBright('TAGGING:        Create annotated git tags                (git tag -a <*name*> -m <*message*>)')) {
                TaggingCreateAnnotatedTags.TaggingCreateAnnotatedTags(hashedDir);
            }
            if (answers.option == chalk.greenBright('TAGGING:        Push git tags to remote                  (git push --tags)')) {
                TaggingPusAllhTagsToRemote.TaggingPusAllhTagsToRemote(hashedDir);
            }
            if (answers.option == chalk.greenBright('TAGGING:        Delete tags                              (git tag -d <*name*>)')) {
                TaggingDeleteTags.TaggingDeleteTags(hashedDir);
            }
            if (answers.option == chalk.magentaBright('REPO:           Register local repo                      (writes repo name in $User\\Documents\\GitRepoList\\List.txt)')) {
                MANAGEMENTRegisterrepo.Registerrepo(hashedDir, answers);
            }
            if (answers.option == chalk.magentaBright('REPO:           Open local repo                          (reads repo name from $User\\Documents\\GitRepoList\\List.txt)')) {
                MANAGEMENTOpenlocalrepo.Openlocalrepo(hashedDir, answers);
            }
            if (answers.option == chalk.magentaBright('REPO:           Go to remote origin                      (git config --get remote.origin.url)')) {
                INFOGotoorigin.GoToOrigin(hashedDir);
            }
            if (answers.option == chalk.hex('#a434eb')('UTILITY:        git config core.autocrlf true            (git config core.autocrlf true)')) {
                UTILITYgitconfigcoreautocrlftrueforWindows.WindowsConfig(hashedDir, answers);          
            }
            if (answers.option == chalk.blackBright('HELP:           Show infographic                         (start http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf )')) {
                HELPShowinfographic.ShowInfographic();
            }
            if (answers.option == chalk.blackBright('HELP:           Show ohshitgit.com                       (start https://ohshitgit.com/ )')) {
                HELPShowohshitgit.ShowOhshitgit();
            }
            if (answers.option ==  chalk.bold.red('API TOKENS:     List API TOKENS')) {
                optionsList.ApiTokenListOptions(hashedDir);
            }
            if (answers.option ==  chalk.bold.red('API TOKENS:     Create API TOKENS')) {
                optionsList.ApiTokenCreateOptions(hashedDir);
            }
            if (answers.option ==  chalk.bold.red('API TOKENS:     Delete API TOKENS')) {
                optionsList.ApiTokenDeleteOptions(hashedDir);
            }
            if (answers.option == gradient.rainbow('SETTINGS:       Modify startup data fields.')) {
                SETTINGSEnableordisablestartupdatafields.SETTINGSFunction1();
            }
            if (answers.option == gradient.rainbow('SETTINGS:       Modify status visualisation types.')) {
                SETTINGSEnableordisableStatusVisualisationTypes.SETTINGSFunction2();
            }
            if (answers.option == gradient.rainbow('SETTINGS:       Modify miscellaneous settings.')) {
                SETTINGSEnableordisableMiscellaneousSettings.SETTINGSFunction3();
            }
            if (answers.option == gradient.rainbow('SETTINGS:       Modify credential-manager settings.')) {
                optionsList.SETTINGS_Credential_Manager(hashedDir);
            }
        });
    }

function SelectNewDir(dir){
    console.log(dir);
    functionsForReadingAndWritingToIniConfigFiles.WriteToFile(dir,'hashed_current_dir',iniFile)
}

module.exports = {
    main:main
  }